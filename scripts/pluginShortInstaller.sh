cordova platform remove ios
cordova platform add ios

bower install ngCordova

#cordova plugin add https://github.com/thedracle/cordova-android-chromeview.git
#cordova plugin add org.apache.cordova.inappbrowser
cordova plugin add org.apache.cordova.console
cordova plugin add org.apache.cordova.camera
cordova plugin add https://github.com/adaptivedev/cordova-plugin-datepicker.git
#cordova plugin add org.apache.cordova.device
#cordova plugin add org.apache.cordova.dialogs
#cordova plugin add org.apache.cordova.file
cordova plugin add https://github.com/EddyVerbruggen/SocialSharing-PhoneGap-Plugin.git
#cordova plugin add org.apache.cordova.statusbar
#cordova plugin add https://github.com/EddyVerbruggen/Toast-PhoneGap-Plugin.git
#cordova plugin add org.apache.cordova.network-information
#cordova plugin add https://github.com/danwilson/google-analytics-plugin.git

#NEED FOR IOS 7 NO KB
cordova plugin add https://github.com/driftyco/ionic-plugins-keyboard.git
#NEED FOR POPUPS FOR EG PUSH HANDLING
cordova plugin add de.appplant.cordova.plugin.local-notification
#USE OUR PUSH PLUGIN
cordova plugin add https://github.com/adaptivedev/PushPlugin.git

cordova platform add ios
cordova platform add android

#cordova -d plugin add ../submodules/adaptivedev/phonegap-facebook-plugin/ --variable APP_ID="625654127541709" --variable APP_NAME="BoosterApp"
cordova -d plugin add ../submodules/pbernasconi/phonegap-facebook-plugin/ --variable APP_ID="625654127541709" --variable APP_NAME="BoosterApp"

bower insstall ngCordova
