angular.module('boosterapp.directives', []);

angular.module('boosterapp.directives')

.directive('baTabsPage', function(baUtil) {
    return {
      restrict: 'EA',
      templateUrl: 'templates/ba-tabs-page.tpl.html',
    }
  })
  .directive('baProfile', function(baUtil, loginService) {
    return {
      restrict: 'EA',
      templateUrl: 'templates/ba-profile.tpl.html',
      controller: function($scope, $state, $stateParams, $window) {
        var getProfileWsr = loginService.getProfileFromWs();
        console.log('startUpcoming:getProfileWsr=' + getProfileWsr);
        $scope.profile = JSON.parse(getProfileWsr);
        console.log('$scope.profile=' + $scope.profile);
        $scope.name = function() {
          return $scope.profile.data.Name;
        }
        $scope.points = function() {
          return $scope.profile.data.LikesTotal;
        }
        $scope.picUrl = function() {
          return $scope.profile.data.PicUrl;
        }
      }
    }
  })
  .directive('baNotifications', function(baUtil, loginService, wsService, PushNotifications) {
    return {
      restrict: 'EA',
      templateUrl: 'templates/ba-notifications.tpl.html',
      controller: function($scope, $state, $stateParams, $window) {
        // $scope.screenHeightString = "200px";
        var userId = loginService.getUserId();
        wsService.getNotification(userId).then(function(result) {
          $scope.UnReadCount = result.data.UnReadCount;
          $scope.items = result.data.Notifications;
          console.log('baNotifications:result.data.Notifications.length=' + result.data.length);
          console.log("baNotifications:$scope.items=" + baUtil.jStr($scope.items));
        });
        $scope.goToItem = function(index) {
          console.log('$scope.items[index]=' + baUtil.jStr($scope.items[index]));
          $state.go('app.goal-page');
          PushNotifications.setItem($scope.items[index]);
        }
        $scope.getText = function(index) {
          if ($scope.items[index].Text == 0 || $scope.items[index].Text == 1) {
            return ' ';
          } else {
            return "'" + $scope.items[index].Text + "'";
          }
        }
      }
    }
  })
  .directive('baMenu', function(baUtil) {
    return {
      restrict: 'EA',
      templateUrl: 'templates/ba-menu.tpl.html',
      controller: function($scope, $state, $stateParams, $window) {
        $scope.screenHeight = $window.innerHeight;
        $scope.headerMarginTop = 0.20 * $scope.screenHeight;
        //$scope.headerPaddingBottom =  0.22 * $scope.screenHeight -  '10px;';
        $scope.firstItemPaddingBottom = (0.20 * $scope.screenHeight) + 'px;';
        // $scope.headerPaddingBottom = (0.20 * $scope.screenHeight) + 'px;';
        // $scope.firstItemPaddingBottom = (0.10 * $scope.screenHeight) + 'px;';
      }
    }
  })
  .directive('baTabs', function(baUtil) {
    return {
      restrict: 'EA',
      template: '<div ng-include="getContentUrl()"></div>',
      // templateUrl: 'templates/ba-likes-list.tpl.html',
      controller: function($scope, $state, $stateParams) {
        var state = $state.$current;
        $scope.tabs = [];
        $scope.onIndex;
        var newIndex = 0;
        $scope.addTab = function() {
          newIndex++;
          $scope.tabs[newIndex] = false;
        }
        $scope.setTabsCurrent = function(i) {
          $scope.onIndex = i;
        }
      }
    }
  })
  .directive('baTab', function(baUtil) {
    return {
      restrict: 'EA',
      require: 'baButtonSet',
      scope: {
        baTabOn: "=",
        baTabOff: "=",
      },
      template: '<div class="homeBtn" background: url("../img/Menu/menu-invite-on.png")>',
      // template: '<img class="homeBtn" src="img/Menu/menu-invite-on.png ">',
      // template: '<div ng-include="getTemplate()"></div>',
      // templateUrl: 'templates/ba-likes-list.tpl.html',
      controller: function($scope, $state, $stateParams) {
        //$scope.myIndex = $scope.addTab();
        $scope.setCurrent = function() {
          $scope.setTabsCurrent(myIndex);
        }
        $scope.getTemplate = function() {
          if ($scope.$parent.onIndex == $scope.myIndex) {
            var rv = 'class={{ba-button-on}}';
          } else {
            var rv = 'class={{ba-button-off}}';
          }
        }
      }
    }
  })
  .directive('baPhotoToShare', function(wsService, baUtil, boostService, imageService) {
    return {
      restrict: 'EA',
      templateUrl: 'templates/ba-photo-to-share.tpl.html',
      controller: function($scope, $state) {
        $scope.getImageBase64 = function() {
          console.log('getImageBase64');
          // if (baUtil.getDevicePlatform() == "iOS") {
          $scope.selfie = imageService.getSelfie();
          return $scope.selfie.image;
          // }
          // if ($scope.selfie)
          // return $scope.selfie.image;
          // return null;
        }
      }
    }
  })
  .directive('baPhotoSharePage', function(wsService, baUtil, boostService, imageService) {
    return {
      restrict: 'EA',
      templateUrl: 'templates/ba-photo-share-page.tpl.html',
      controller: function($scope, $state, $rootScope) {
        $scope.boost = boostService.getBoost();

        $scope.unbind = $rootScope.$on('setBoost', function() {
          $scope.boost = boostService.getBoost();
        });
        $scope.$on('$destroy', $scope.unbind);

        $scope.cancel = function() {
          $state.go('app.goals-world');
        }
        $scope.getBoost = function() {
          $scope.boost = boostService.getBoost();
          console.log('$scope.boost=' + baUtil.jStr($scope.boost));
        }
        $scope.sendPhoto = function() {
          var selfie = imageService.getSelfie();
          wsService.sendSelfie($scope.boost.BoostId, $scope.taValue, selfie.privacy, selfie.image).then(function(result) {
            console.log('sendSelfie:result=' + baUtil.jStr(result));
            $state.go('app.goals-world');
          })
        }
      }
    }
  })
  // .directive('baSummary', function(boostService) {
  //   return {
  //     restrict: 'EA',
  //     templateUrl: 'templates/ba-summary.tpl.html',
  //     controller: function($scope, $state, $stateParams, wsService) {
  //       $scope.chooseBooster = function() {
  //         $state.go('app.choose-booster');
  //       }
  //       $scope.sendBoost = function() {
  //         var date = new Date();
  //         wsService.addBoost(loginService.getUserId(), $scope.boost.booster.FbId, new Date(), -120, $scope.boost.text, -15).then(function(result) {
  //           $state.go('app.goals-my');
  //         });
  //       };
  //     }
  //   }
  // })
  // .directive('baTakeSelfie', function() {
  //   return {
  //     link: function($scope, $element, $attrs) {},
  //     templateUrl: 'templates/add-boost-btn.tpl.html',
  //     controller: function($scope, $q, $rootScope, $state, $cordovaCamera, $cordovaToast, $cordovaFile, wsService, baUtil) {
  //       baUtil.trackEvent("TakeSelfieBoostCtrl");
  //       selfieService.initOptions();
  //       selfieService.takeSelfie();
  //     }
  //   }
  // })
  .directive('baGoalUpcoming', function(imageService, boostService, wsService, baUtil, loginService) {
    return {
      restrict: 'EA',
      templateUrl: 'templates/ba-goal-upcoming.tpl.html',
      controller: function($scope, $state) {
        var profileWsr = loginService.getProfileFromWs();
        var jsonProfileWsr = JSON.parse(profileWsr);
        console.log('jsonProfileWsr=' + baUtil.jStr(jsonProfileWsr));
        var myUserId = loginService.getUserId();
        $scope.isMyGoal = function() {
          if (myUserId != $scope.item.Boost.UserId) {
            return false;
          }
          return true;
        }
        $scope.sendBoost = function() {
          wsService.sendPoke($scope.item.Boost.BoostId).then(function(result) {
            alert('Boost sent');
          })
        }
        $scope.takePhoto = function() {
          var boost = $scope.item.Boost;
          console.log('boost=' + baUtil.jStr(boost));
          boostService.setBoost(boost);
          try {
            imageService.initOptions();
            imageService.takePhoto();
            $state.go('app.photo-share-page');
          } catch (exc) {
            console.log('takePhoto:exc=' + baUtil.jStr(exc));
            if (!baUtil.devMode()) {
              return;
            }
            // an idea for web version: imageService.chooseFile();
            var url = "https://gist.githubusercontent.com/adaptivedev/a46253228667aa1c278d/raw/cda23d08ab14e937cc129616e59ffd1bde9844cc/girlcliffjump.base64";
            url = "https://gist.githubusercontent.com/adaptivedev/964d736652b7d44ed536/raw/347ef2acd7e6ec3ebe2a331b924048dbde522013/hottie.base64";
            wsService.getUrl(url).then(function(result) {
              console.log('getUrl:result.data.length=' + result.data.length);
              imageService.setSelfie(result.data);
              var testSelfie = imageService.getSelfie();
              // if (testSelfie.image == "") alert('testSelfie.image=' + testSelfie.image);
              $state.go('app.photo-share-page');
            }).catch(function(exc) {
              alert('getUrl:exc=' + exc);
            });
          }
        }
      }
    }
  })
  .directive('baUserRow', function(boostService, baUtil) {
    return {
      restrict: 'EA',
      templateUrl: 'templates/ba-user-row.tpl.html',
      require: 'baChooseBooster',
      controller: function($scope, $state) {
        $scope.clickedBooster = function() {
          $scope.booster = $scope.user;
          console.log('$scope.user=' + baUtil.jStr($scope.user));
          boostService.setBooster($scope.booster);
          $scope.booster = boostService.getBoost().booster;
          if (baUtil.d() > 5) console.log('baUserRow:$scope.booster=' + baUtil.jStr($scope.booster));
          $state.go('app.new-goal-summary');
        }
      }
    }
  })
  .directive('baChooseBooster', function(wsService, loginService, baUtil) {
    return {
      restrict: 'EA',
      templateUrl: 'templates/ba-choose-booster.tpl.html',
      controller: function($scope) {
        $scope.friends = [];
        $scope.booster;
        $scope.getFriends = function() {
            var fbToken = loginService.getFbToken();
            wsService.getFriends(fbToken).then(function(result) {
              $scope.friends.push.apply($scope.friends, result.data);
              if (baUtil.d() > 5) console.log('$scope.friends=' + baUtil.jStr($scope.friends));
            });
          }
          // $scope.clickedBooster = function(index) {
          //   // alert('baChooseBooster:clickedBooster:index=' + index);
          //   $scope.booster = friends[index];
          // }
      }
    }
  })
  .directive('baGoalCompleted', function() {
    return {
      restrict: 'EA',
      templateUrl: 'templates/ba-goal-completed.tpl.html',
    }
  })
  .directive('baImageNew', function(baUtil, wsService, PushNotifications, loginService) {
    return {
      restrict: 'EA',
      templateUrl: 'templates/ba-image-new.tpl.html',
      controller: function($scope, $rootScope) {
        $scope.unbind = $rootScope.$on('pushSetItem', function() {
          $scope.item = PushNotifications.getItem();
          $scope.sid = $scope.item.SelfieId;
          var fbToken = loginService.getFbToken();
          wsService.getSelfieById($scope.sid, 123, true, fbToken).then(function(result) {
            $scope.selfie = result.data;
            $rootScope.$emit('newImageReceived');
          });
        });
        $scope.$on('$destroy', $scope.unbind);
      }
    }
  })
  .directive('baImages', function(baUtil, loadService, loginService, likeService, wsService, imageService, PushNotifications) {
    return {
      restrict: 'EA',
      templateUrl: 'templates/ba-images.tpl.html',
      scope: {
        selfies: '=selfies',
      },
      link: function(scope, element, attrs) {
        scope.likeCount = scope.selfies[0].ListSelfieUsersLike.length;
        likeService.setLikeCount(sid, scope.likeCount);
        scope.selfie = scope.selfies[0].ListBoostSelfie[0];
        likeService.updateSelfies(scope.selfies);
        var sid = scope.selfie.SelfieId;
        var boostId = scope.selfie.BoostId;
        if (baUtil.d() > 15) console.log('baImages:link:sid=' + sid);
        var apiPf = baUtil.partial(wsService.getSelfie, boostId);
        loadService.loadMore(apiPf).then(function(result) {
          scope.addResponse(result);
        });
      },
      controller: function($scope, $state, $stateParams, $location) {
        $scope.sid = $scope.selfies[0].ListBoostSelfie[0].SelfieId;
        var userId = loginService.getProfile().UserId;
        $scope.update = function() {
          console.log("$state.current.url=" + $state.current.url);
          if ($state.current.url == '/goal-page') {
            $scope.selfie = PushNotifications.getItem().ListSelfieCommentsLikes[0].ListBoostSelfie[0];
            // console.log('$scope.selfie=' + baUtil.jStr($scope.selfie));
          }
        }
        $scope.addResponse = function(result) {
          $scope.selfie = result.data;
          // $scope.$broadcast('addGoals.infiniteScrollComplete');
          // $scope.$apply();
        }
      }
    }
  })
  // .directive('baLike', function() {
  //   return {
  //     restrict: 'EA',
  //     templateUrl: 'templates/ba-like.tpl.html',
  //   }
  // })
  // .directive('baLikeCount', function() {
  //   return {
  //     restrict: 'EA',
  //     require: "baLikesNum",
  //     templateUrl: 'templates/ba-likes-num.tpl.html'
  //   }
  // })
  .directive('baLikesTeaser', function(wsService, loginService, likeService, loadService, baUtil) {
    return {
      restrict: 'EA',
      templateUrl: 'templates/ba-likes-teaser.tpl.html',
      controller: function($scope, $state, $stateParams, $rootScope) {
        $scope.unbind = $rootScope.$on('newImageReceived', function() {
          $scope.updateLikesCount();
        });
        $scope.$on('$destroy', $scope.unbind);
        $scope.unbind = $rootScope.$on('selfieLikeCountChanged', function() {
          $scope.updateLikesCount();
        });
        $scope.$on('$destroy', $scope.unbind);
        $scope.getLikeCount = function() {
          return $scope.likeCount;
        }
        $scope.updateLikesCount = function() {
          var fbToken = loginService.getFbToken();
          wsService.getSelfieLikes($scope.sid, fbToken, 6, 0).then(function(result) {
            console.log('baLikesTeaser:updateLikesCount:result=' + baUtil.jStr(result));
            $scope.likeCount = result.data.length;
            $scope.likes = result.data;
            var callerLikes = 0;
            if (result.data.length > 0) {
              callerLikes = result.data[0].doesCallerLike;
            }
            var iLike;
            if (callerLikes == loginService.getUserId()) {
              iLike = true;
            }
            var updateServer = false;
            likeService.setLike($scope.sid, iLike, updateServer);
          });
          $scope.likeCount = likeService.getLikeCount($scope.sid);
        }
      }
    }
  })
  .directive('baLikesNum', function(wsService, loginService, likeService, loadService, baUtil) {
    return {
      restrict: 'EA',
      templateUrl: 'templates/ba-likes-num.tpl.html',
      scope: {
        selfie: '=selfie'
          //likers: '=likers',
          // likecount: '=likecount',
          // selfieid: '@selfieid',
      },
      controller: function($scope, $state, $stateParams, $rootScope) {
        $scope.sid = $scope.selfie.ListBoostSelfie[0].SelfieId;
        $scope.liked = $scope.selfie.ListBoostSelfie[0].CallerLikes;
        $scope.likeClasses = likeService.getLikeClass($scope.liked);
        $scope.likers = $scope.selfie.ListSelfieUsersLike;
        // if ($scope.likers[0] === undefined) return;
        $scope.getLikeCount = function() {
          $scope.likeCount = likeService.getLikeCount($scope.sid);
          return $scope.likeCount;
        }
        $scope.unbind = $rootScope.$on('setLikeCount', function() {
          $scope.likeCount = likeService.getLikeCount($scope.sid);
        });
        $scope.$on('$destroy', $scope.unbind);
        $scope.addResponse = function(result) {
          $scope.likers = result;
        }
      }
    }
  })
  .directive('baLikeButtonNew', function(wsService, likeService, loginService) {
    return {
      restrict: 'EA',
      templateUrl: 'templates/ba-like-button.tpl.html',
      controller: function($scope, $rootScope, $state, $stateParams) {
        $scope.unbind_setLike = $rootScope.$on('setLike', function() {
          $scope.liked = likeService.getLike($scope.sid);
          $scope.likeClasses = likeService.getLikeClass($scope.liked);
        });
        $scope.$on('$destroy', $scope.unbind_setLike);
        $scope.flipLike = function() {
          likeService.flipLike($scope.sid);
        }
      }
    }
  })
  .directive('baLikeButton', function(wsService, likeService, loginService) {
    return {
      restrict: 'EA',
      scope: {
        selfie: '=selfie'
          // sid: '=sid',
          // liked: "=liked"
      },
      templateUrl: 'templates/ba-like-button.tpl.html',
      controller: function($scope, $rootScope, $state, $stateParams) {
        $scope.sid = $scope.selfie.ListBoostSelfie[0].SelfieId;
        $scope.liked = $scope.selfie.ListBoostSelfie[0].CallerLikes;
        $scope.likeClasses = likeService.getLikeClass($scope.liked);
        $scope.unbind = $rootScope.$on('setLike', function() {
          $scope.liked = likeService.getLike($scope.sid);
          $scope.likeClasses = likeService.getLikeClass($scope.liked);
        });
        $scope.$on('$destroy', $scope.unbind);
        $scope.flipLike = function() {
          likeService.flipLike($scope.sid);
        }
      }
    }
  })
  .directive('baLikesList', function(loadService, likeService, loginService, wsService, baUtil) {
    return {
      restrict: 'EA',
      // template: '<div ng-include="getContentUrl()"></div>',
      templateUrl: 'templates/ba-likes-list.tpl.html',
      controller: function($scope, $state, $stateParams) {
        $scope.addResponse = function(result) {
          $scope.likers = result.data;
          likeService.setLikeCount(sid, $scope.likers.length);
        }
        var sid = $stateParams.selfieId;
        var apiPf = baUtil.partial(wsService.getSelfieLikes, sid, loginService.getFbToken(), -1, -1);
        loadService.loadMore(apiPf).then(function(result) {
          $scope.addResponse(result);
        });
        $scope.haveMoreDataToLoad = function() {
          if (!$scope.likers) return false;
          var rows = $scope.likers.length;
          var count = likeService.getLikeCount();
          if (rows == count) return false;
        }
      }
    }
  })
  .directive('baLikeRow', function(baUtil) {
    return {
      restrict: 'EA',
      templateUrl: 'templates/ba-like-row.tpl.html',
    }
  })
  .directive('baCommentsTeaserNew', function($rootScope, baUtil, wsService, loginService, likeService) {
    return {
      restrict: 'EA',
      templateUrl: 'templates/ba-comments-teaser.tpl.html',
      controller: function($scope) {
        wsService.getSelfieComments($scope.sid, loginService.getFbToken()).then(function(result) {
          $scope.comments = result.data;
        })
        $scope.unbind = $rootScope.$on('newImageReceived', function() {
          $scope.update();
        });
        $scope.unbind = $rootScope.$on('commentAdded', function() {
          $scope.update();
        });
        $scope.update = function() {
          wsService.getSelfieComments($scope.sid, loginService.getFbToken()).then(function(result) {
            $scope.comments = result.data;
          })
          $scope.likeCount = likeService.getLikeCount($scope.sid);
        }
        $scope.$on('$destroy', $scope.unbind);
      }
    }
  })
  .directive('baCommentsTeaser', function($rootScope, baUtil, wsService, loginService, likeService) {
    return {
      restrict: 'EA',
      templateUrl: 'templates/ba-comments-teaser.tpl.html',
      scope: {
        sid: "=sid"
      },
      link: function(scope, element, attr) {
        element.on('load', scope.loadHandler);
      },
      controller: function($scope) {
        $scope.loadHandler = function() {
          alert('loadHandler');
        }
        wsService.getSelfieComments($scope.sid, loginService.getFbToken()).then(function(result) {
          $scope.comments = result.data;
        })
        $scope.unbind = $rootScope.$on('commentAdded', function() {
          wsService.getSelfieComments($scope.$parent.sid, loginService.getFbToken()).then(function(result) {
            $scope.comments = result.data;
          })
          $scope.likeCount = likeService.getLikeCount($scope.sid);
        });
        $scope.$on('$destroy', $scope.unbind);
      }
    }
  })
  .directive('baCommentRow', function(baUtil) {
    return {
      restrict: 'EA',
      templateUrl: 'templates/ba-comment-row.tpl.html',
    }
  })
  .directive('baCommentsList', function($rootScope, baUtil, wsService, loginService, loadService) {
    return {
      restrict: 'EA',
      templateUrl: 'templates/ba-comments-list.tpl.html',
      controller: function($scope, $state, $stateParams, $ionicScrollDelegate) {
        $scope.addResponse = function(result) {
          $scope.commentsCount = result.data.TotalComments;
          $scope.comments = result.data.Comments;
          $ionicScrollDelegate.scrollBottom();
          $scope.text = '';
          $rootScope.$emit('commentAdded');
        }
        var sid = $stateParams.selfieId;
        var apiPf = baUtil.partial(wsService.getSelfieComments, sid, loginService.getFbToken(), -1, -1);
        loadService.loadMore(apiPf).then(function(result) {
          $scope.addResponse(result);
        });

        $scope.list = [];
        $scope.text = '';
        $scope.submit = function() {
          if ($scope.text) {
            var fbId = loginService.getFbToken();
            wsService.sendComment(sid, fbId, $scope.text).then(function(result) {
              if (baUtil.d() > 5) console.log('baCommentsList:sendComment:result=' + baUtil.jStr(result));
              loadService.loadMore(apiPf).then(function(result) {
                $scope.addResponse(result);
              });
              // $timeout(function() {
              //   $window.location.reload();
              // }, 1000);
            }, function(err) {
              console.log('baCommentsList:sendComment:err=' + baUtil.jStr(err));
            }).catch(function(exc) {
              console.log('baCommentsList:sendComment:exc=' + baUtil.jStr(exc));
            });
            // $window.location.reload();
            // $scope.comments.push(this.text);
            // $scope.text = '';
          }
        };
        $scope.haveMoreDataToLoad = function() {
          if (!$scope.comments) return false;
          var rows = $scope.comments.length;
          var count = $scope.commentsCount;
          if (rows == count) return false;
        }
      }
    }
  })
  .directive('tabsBottom', function(stateService) {
    return {
      restrict: 'AE',
      templateUrl: 'templates/tabs-bottom.tpl.html',
      controller: function($scope, $rootScope) {
        $scope.unbind = $rootScope.$on('tabsStateChanged', function() {
          $scope.state = stateService.getState();
          $scope.mytitle = stateService.getTitle();
          $scope.setHeights();
          console.log('tabsBottom: $scope.state=' + $scope.state);
        });
        $scope.$on('$destroy', $scope.unbind);
        $scope.setState = function(theState) {
          stateService.setState(theState);
        }
        $scope.getIsPublic = function() {
          var rv = stateService.getIsPublic();
          return rv;
        }
        $scope.setHeights = function() {
          var state = stateService.getState();
          if (state == "app.goals-world") {
            // $scope.screenHeight = $window.innerHeight - 158;
            $scope.topTabsHeight = 35;
          } else {
            // $scope.screenHeight = $window.innerHeight - 58;
            $scope.topTabsHeight = 0;
          }
          // $scope.screenHeightString = $scope.screenHeight + 'px;';
        }

      }
    }
  })
  .directive('addBoostBtn', function(boostService) {
    return {
      restrict: 'AE',
      link: function(scope, element, attrs) {},
      templateUrl: 'templates/add-boost-btn.tpl.html',
      controller: function($scope, $state, $stateParams) {
        var state = $state.$current;
        $scope.getClass = function() {
          var classes = {};
          console.log('state' + state);
          console.log('$scope.showBoostBtn' + $scope.showBoostBtn);
          if (boostService.isSet()) {
            // if (state == 'app.new-goal-summary-finished') {
            classes['add-boost-btn-on'] = true;
          } else {
            classes['add-boost-btn-off'] = true;
          }
          console.log('classes=' + JSON.stringify(classes));
          return classes;
        }
      }
    }
  })

.directive('baGoals', function(baUtil, loginService, wsService, stateService) {
  return {
    restrict: 'AE',
    templateUrl: 'templates/ba-goals.tpl.html',
    scope: {
      who: "@who",
    },
    // link: function(scope, element, attrs) {
    // 	scope.who;
    // }
    controller: function($scope, $attrs, $rootScope, $state, $window, $ionicScrollDelegate, PushNotifications) {
      var loadMoreSemaCount = 0;
      $scope.goals = [];
      $scope.StatusToGet = "C";
      $scope.withUpcoming = true;
      $scope.withImages = false;
      $scope.topN = 1;
      $scope.skipN = 0;
      var fbToken = loginService.getFbToken();

      $scope.testPush = function(index) {
        var item = $scope.goals[index];
        PushNotifications.setItem(item);
        $state.go('app.goal-page');
      }

      $scope.doRefresh = function() {
        $scope.loadMoreSemaCount = 0;
        $scope.skipN = 0;
        $scope.goals = [];
        $ionicScrollDelegate.scrollTop(false);
      }

      $scope.unbind = $rootScope.$on('tabsStateChanged', function() {
        $scope.goalTypesToGet = $scope.who;
        if ($scope.goals.length == 0) {
          $scope.loadMore();
        }
      });
      $scope.$on('$destroy', $scope.unbind);

      $rootScope.$on('$stateChangeStart',
        function(event, toState, toParams, fromState, fromParams) {
          console.log('baGoals:stateChangeStart:fromState.url=' + fromState.url + ";toState.url=" + toState.url);
          $scope.goalTypesToGet = $scope.who;
        });

      $rootScope.$on('$stateChangeSuccess',
        function(event, toState, toParams, fromState, fromParams) {
          console.log('baGoals:stateChangeSuccess:fromState.url=' + fromState.url + ";toState.url=" + toState.url);
        });

      $scope.unbind = $rootScope.$on('fbTokenSet', function() {
        fbToken = loginService.getFbToken();
        if ($scope.goals.length == 0) {
          $scope.loadMore();
        }
      });
      $scope.$on('$destroy', $scope.unbind);

      // $scope.$on('$stateChangeSuccess', function() {
      //$scope.loadMore();
      // });

      // stateService.setState($scope.state);
      // var who = stateService.getWho();
      // callGetAllBoosts(who);

      $scope.loadMore = function() {
        // var temp = stateService.getWho();
        var who = $scope.who;
        fbToken = loginService.getFbToken();
        if (baUtil.d() > 5) console.log('baGoals:loadMore:topN=' + $scope.topN);
        if (baUtil.d() > 5) console.log('baGoals:loadMore:loadMoreSemaCount=' + loadMoreSemaCount);
        if (loadMoreSemaCount > 0) {
          return;
        }
        if (!baUtil.varIsSet(fbToken)) {
          console.log('baGoals:loadMore:!isVarSet(fbToken)');
          return;
        }
        loadMoreSemaCount++;
        wsService.getAllBoosts(who, $scope.StatusToGet, $scope.withUpcoming, $scope.topN, $scope.skipN, fbToken).then(function(result) {
          loadMoreSemaCount--;
          if (baUtil.d() > 5000) console.log('baGoals:loadMore:result=' + baUtil.jStr(result));
          if (result === undefined || result.data == "END OF LIST" || result.data.length === undefined) {
            $scope.goalTypesToGet = "NONE";
            if (baUtil.d() > 5) console.log('baGoals:loadMore:result=' + baUtil.jStr(result));
            return;
          }
          $scope.addGoals(result);
          if (result.data.length > 0) {
            $scope.$broadcast('scroll.infiniteScrollComplete');
          }
        }).catch(function(exc) {
          console.log('baGoals:catch:exc=' + baUtil.jStr(exc));
          if (exc.data.Message == "An error has occurred.") {
            $scope.goalTypesToGet = "NONE";
          }
        });
      };

      $scope.addGoals = function(result) {
        var v = result; // OData:result.data.value;
        if (baUtil.jStr() > 5) console.log('baGoals:addGoals:v.length=' + v.length);
        if (v.data.length == 0) {
          if (StatusToGet == "A") {
            StatusToGet = "C";
            console.log("baGoals:addGoals:END:StatusToGet=" + StatusToGet);
          }
        }
        if (baUtil.jStr() > 5) console.log("baGoals:addGoals:$scope.goals.length=" + $scope.goals.length);
        for (var i = 0; i < v.data.length; i++) {
          var indexOfElement = $scope.goals.indexOf(v.data[i]);
          console.log("baGoals:addGoals:indexOfElement=" + indexOfElement);
          if (indexOfElement >= 0) {
            //$scope.goalTypesToGet = "NONE";
            return;
          }
          $scope.goals.push(v.data[i]);
          $scope.skipN++;
        }
      }
      $scope.showWorldFilter = function() {
        return stateService.getIsShowWorldFilter();
      }

      $scope.callGetAllBoosts = function(who) {
        apiPf = baUtil.partial(wsService.getAllBoosts, $scope.who, $scope.withUpcoming, $scope.withImages, $scope.topN, $scope.skipN, fbToken);
        // handlerFn = loadService.getResultData;
        cbFn = $scope.addGoals;
      }
      $scope.moreDataCanBeLoaded = function() {
        var who = $scope.who;
        if (baUtil.d() > 50) console.log('baGoals:moreDataCanBeLoaded:$scope.goalTypesToGet=' + $scope.goalTypesToGet + ';who=' + who);
        if ($scope.goalTypesToGet == "NONE") {
          //return false;
        }
        return true;
      }
    }
  }
})

// .directive('baGoalsMy', function() {
//     return {
//       restrict: 'AE',
//       templateUrl: 'templates/ba-goal.tpl.html',
//       require: 'baGoals',
//       controller: function($scope) {
//         $scope.who = "My";
//       }
//     }
//   })
//   .directive('baGoalsWorld', function() {
//     return {
//       restrict: 'AE',
//       templateUrl: 'templates/ba-goal.tpl.html',
//       require: 'baGoals',
//       controller: function($scope) {
//         $scope.who = "World";
//       }
//     }
//   })
//   .directive('baGoalsBoostees', function() {
//     return {
//       restrict: 'AE',
//       templateUrl: 'templates/ba-goal.tpl.html',
//       require: 'baGoals',
//       controller: function($scope) {
//         $scope.who = "Boostees";
//       }
//     }
//   })

// .config(function($provide) {

//   $provide.decorator('baGoalsDirective', function($delegate) {

//     var directive = $delegate[0];

//     angular.extend(directive.scope, {
//       who: '@'
//     });

//     return $delegate;
//   });
// })

.directive('baGoalPage', function(baUtil, PushNotifications, $rootScope, wsService, loginService) {
  return {
    restrict: 'AE',
    controller: function($scope) {
      // $scope.item = PushNotifications.getItem();
      $scope.unbind = $rootScope.$on('pushSetItem', function() {
        $scope.item = PushNotifications.getItem();
        $scope.update();
      });
      $scope.$on('$destroy', $scope.unbind);
      $scope.update = function() {
        $scope.item = PushNotifications.getItem();

        var i = $scope.item;
        i.UserPicUrl = i.ProfilePic;
        i.UserName = i.BoosterFirstName + " " + i.BoosterLastName;
        i.RequestUserName = i.UserFirstName + " " + i.UserLastName;
        // o.Boost.RequestUserName = i.BoosterFirstName + " ";
        // i.BoosterLastName;

        console.log('baGoalPage:update:$scope.item=' + baUtil.jStr($scope.item));

        if ($scope.item.SelfieId > 0) {
          var sid = $scope.item.SelfieId;
          var fbToken = loginService.getFbToken();
          wsService.getSelfieById(sid, 123, true, fbToken).then(function(result) {
            if (baUtil.d() > 5000) console.log('baGoalPage:getSelfieById:result=' + baUtil.jStr(result));
            $scope.selfie = result.data;
          })
        }

      }
    },
    templateUrl: 'templates/ba-goal-PAGE.tpl.html'
  }
})

.directive('baGoal', function() {
    return {
      restrict: 'AE',
      templateUrl: 'templates/ba-goal.tpl.html'
    }
  })
  .directive('baTopBar', function(imageService, stateService) {
    return {
      restrict: 'AE',
      link: function(scope, element, attrs) {},
      templateUrl: 'templates/ba-top-bar.tpl.html',
      controller: function($scope) {
        $scope.OneOn = 'worldTabOn';
        $scope.OneOff = 'worldTabOff';
        $scope.TwoOn = 'friendsTabOn';
        $scope.TwoOff = 'friendsTabOff';
        $scope.isPublic = stateService.getIsPublic();
        //imageService.selfie.isPublic = $scope.isPublic;
        $scope.setPublic = function(b) {
          $scope.isPublic = b;
          stateService.setPublic(b);
          //imageService.selfie.isPublic = b;
        }
        $scope.getWorldClass = function() {
          // console.log('getWorldClass');
          var classes = {};
          if ($scope.isPublic) {
            classes[$scope.OneOn] = true;
          } else {
            classes[$scope.OneOff] = true;
          }
          // console.log('getWorldClass=' + JSON.stringify(classes, undefined, 2));
          return classes;
        }
        $scope.getFriendsClass = function() {
          // console.log('getFriendsClass');
          var classes = {};
          if (!$scope.isPublic) {
            classes[$scope.TwoOn] = true;
          } else {
            classes[$scope.TwoOff] = true;
          }
          // console.log('getFriendsClass=' + JSON.stringify(classes, undefined, 2));
          return classes;
        }
      }
    }
  })
  .directive('baSharePrivacy', function(imageService) {
    return {
      restrict: 'AE',
      link: function(scope, element, attrs) {},
      templateUrl: 'templates/ba-share-privacy.tpl.html',
      controller: function($scope) {
        $scope.OneOn = 'worldSectionOn';
        $scope.OneOff = 'worldSectionOff';
        $scope.TwoOn = 'friendsSectionOn';
        $scope.TwoOff = 'friendsSectionOff';
        $scope.isPublic = true;
        //imageService.selfie.isPublic = $scope.isPublic;
        $scope.setPublic = function(b) {
          $scope.isPublic = b;
          var privacy = 'w';
          if (!b) privacy = 'f';
          imageService.setPrivacy(privacy);
          //imageService.selfie.isPublic = b;
        }
        $scope.getWorldClass = function() {
          // console.log('getWorldClass');
          var classes = {};
          if ($scope.isPublic) {
            classes[$scope.OneOn] = true;
          } else {
            classes[$scope.OneOff] = true;
          }
          // console.log('getWorldClass=' + JSON.stringify(classes, undefined, 2));
          return classes;
        }
        $scope.getFriendsClass = function() {
          // console.log('getFriendsClass');
          var classes = {};
          if (!$scope.isPublic) {
            classes[$scope.TwoOn] = true;
          } else {
            classes[$scope.TwoOff] = true;
          }
          // console.log('getFriendsClass=' + JSON.stringify(classes, undefined, 2));
          return classes;
        }
      }
    }
  })
  .directive('newGoalSummary', function($ionicNavBarDelegate) {
    return {
      restrict: 'AE',
      templateUrl: 'templates/new-goal-summary.tpl.html',
      controller: function($scope, $rootScope, $state, $stateParams, wsService, boostService) {
        // $scope.sendBoost = function() {
        //   var date = new Date();
        //   wsService.addBoost(loginService.getUserId(), $scope.boost.booster.FbId, new Date(), -120, $scope.boost.text, -15).then(function(result) {
        //     $state.go('app.goals-my');
        //   });
        // };
        $scope.cancel = function() {
          // $ionicNavBarDelegate.back();
          $state.go('app.goals-world');
        }
        $scope.getBoosterName = function() {
          if (!$scope.boost.booster.PicUrl) {
            $scope.boosterName = "Choose Friend";
          } else {
            $scope.boosterName = $scope.boost.booster.FirstName + " " + $scope.boost.booster.LastName;
          }
          return $scope.boosterName;
        }
        $scope.getBoosterPicClasses = function() {
          // console.log('getWorldClass');
          var classes = {};
          if (!$scope.boost.booster.PicUrl) {
            classes['boosterImg'] = true;
          } else {
            classes['myRound'] = true;
            classes['boosterImgProfilePic'] = true;
          }
          // console.log('getWorldClass=' + JSON.stringify(classes, undefined, 2));
          return classes;
        }
        $scope.getBoosterPic = function() {
          var b = $scope.boost.booster.PicUrl;
          if (!b) return null;
          var rv = {
            background: "url('" + $scope.boost.booster.PicUrl + "')"
          }
          console.log('rv=' + JSON.stringify(rv));
          return rv;
        }
      }
    }
  })
