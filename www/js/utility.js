angular.module('boosterapp.utility', ['ionic'])

//======================================= UTILITY SERVICE =================================
.service('baUtil', function() {

  // debug level
  // 0-10 log importance -- error = 0
  // *10 if in a loop
  // *10 if its a large block (eg JSON)
  // *10 if its a block of blocks
  this.d = function() {
    return 150;
  }

  this.devMode = function() {
    return true;
  };

  this.jStr = function(jsonObj) {
    return JSON.stringify(jsonObj, null, 2);
  }

  this.varIsSet = function(x) {
    if (x == "" || x == null || x == undefined || x == "null" || x == "undefined") {
      return false;
    }
    return true;
  }

  this.partial = function(func) {
    var args = Array.prototype.slice.call(arguments, 1);
    return function() {
      var allArguments = args.concat(Array.prototype.slice.call(arguments));
      return func.apply(this, allArguments);
    };
  }

  var dp;
  var dpnp;
  var dpp;
  var dppd;
  this.setDevicePlatform = function(ionicPlatform) {
    dp = ionicPlatform;
    dpnp = dp.navigator.platform;
    dpp = dp.platform();
    dppd = dpp.device;
    try {
      window.localStorage['dppd'] = dppd;
    } catch (exc) {
      window.localStorage['dppd'] = exc;
    }
  }

  this.getDevicePlatform = function() {
    var d = window.localStorage['dppd'];
    if (dp === undefined) return d;
    if (dp.isIOS()) return "iOS";
    if (dp.isAndroid()) return "Android";
    if (dp.isWebView()) return "WebView";
    return d;
  }

  //----------- BEHAVIOR TRACKING ----------

  var d = "x";
  var use_analytics = "";
  this.init = function(devicePlatform) {
    d = devicePlatform;
  }

  if (!d) alert('must call baUtil.init(<devicePlatform>);');

  this.trackEvent = function(act) {
    if (d == "Android") d = "A";
    if (d == "iOS") d = "i";

    var page = d + ':' + act;
    console.log('trackEvent:page=' + page);
    if (use_analytics) analytics.trackView(page);

    //gaPlugin.trackPage(nativePluginResultHandler, nativePluginErrorHandler, page);

    console.log('mixpanel.track:' + d + ":" + act);
    mixpanel.track(d + ":" + act);

    if (this.varIsSet(ga)) {
      ga('send', 'screenview', {
        'screenName': act
      });

      //GA 
      ga('send', 'screenview', {
        'appName': 'Booster_WebApp',
        'appId': 'myAppId',
        'appVersion': '1.6.6',
        'appInstallerId': 'myInstallerId',
        'screenName': act
      });

      ga('send', {
        'hitType': 'pageview',
        'page': act,
        'title': act
      });
    }
  }

  this.trackError = function(screenName, errorMessage) {
    this.trackEvent("Error:" + screenName + ':' + errorMessage);
  }

  this.trackUserError = function(act) {
    this.trackEvent("UserError:" + act);
  }

  this.trackSendSelfie = function(comment, boostId) // TODO: change to toUserId
    {
      if (use_analytics) analytics.trackEvent('Comment', this.profile.UserId, comment);
      if (baUtil.varIsSet(ga)) ga('send', 'Comment', this.profile.UserId, comment);
    }

  this.trackReview = function(comment, isLovedIt) // TODO: add toUserId
    {
      if (use_analytics) analytics.trackEvent('Comment', this.profile.UserId, comment, isLovedIt);
      ga('send', 'Comment', this.profile.UserId, comment, isLovedIt);
    }

})

//=================================== LOGIN SERVICE ========================================

.service('loginService', function($ionicPlatform, $ionicPopup, $q, $state, $rootScope, baUtil, wsService, $cordovaFacebook, $timeout) {

  var use_analytics = false;

  self = this;
  this.pushState = "";
  this.userProfile = {};
  this.getProfile = function() {
    return self.userProfile;
  }

  this.checkPlugin = function(var1) {
    console.log('var=' + var1);
  }

  this.popup = function(title, message) {
    console.log('popup:title=' + title + ' ;message=' + message);
    /*
     $ionicPopup.show({
         title: title,
         template: message
     });
    */
  }

  this.goToState = function(theState) {
    $state.go(theState);
  }

  this.doDelayed = function(f1, delay, args) {
    console.log('doDelayed:args=' + JSON.stringify(args));

    function sleep(millis, callback) {
      setTimeout(function() {
        callback();
      }, millis);
    }

    function foobar_cont() {
      console.log("foobar_cont");
      console.log('doDelayed:foobar_cont:args=' + JSON.stringify(args));
      f1.apply(this, args);
    };
    sleep(delay, foobar_cont);
  }

  this.fbShare = function(selfie) {
    var d = $q.defer();
    console.log('this.fbShare:selfie.UserId=' + selfie.UserId);
    baUtil.trackEvent("fbShare");

    var message = selfie.message, //selfie.BoostTitle,
      subject = 'Booster App',
      fileOrFileArray = "data:image/png;base64," + selfie.image,
      url = "http://signup.theboosterapp.com";

    var sharing = window.plugins.socialsharing;
    sharing.share(
      message,
      subject,
      fileOrFileArray,
      url,
      function(result) {
        baUtil.trackEvent('fbShare:success');
        console.log('Sharing success: ' + JSON.stringify(result));
        d.resolve(result);
        return d.promise;
      },
      function(result) {
        baUtil.trackError(error, "fbShare");
        console.log('Sharing error: ' + JSON.stringify(result));
        d.reject(result);
        return d.promise;
      });
    return d.promise;
  }

  //FB login button click event
  this.promiseFacebookLogin = function() {
    self = this;
    baUtil.trackEvent('promiseFacebookLogin');
    console.log('promiseFacebookLogin');
    var d = $q.defer();
    var dp = this.getDevicePlatform();
    $cordovaFacebook.
    login
      (
        ['public_profile', 'user_friends', 'email'],
        // SUCCESS
        function(res) {
          console.log('promiseFacebookLogin:$cordovaFacebook:login:success');
          baUtil.trackEvent('$cordovaFacebook.login:success');
          console.log("promiseFacebookLogin:res=" + JSON.stringify(res));
          alert('promiseFacebookLogin:success:res.authResponse.accessToken=' + res.authResponse.accessToken);
          self.setFbToken(res.authResponse.accessToken);
          d.resolve(res);
          return d.promise;
        },
        function(err) {
          var errLog = 'promiseFacebookLogin:$cordovaFacebook:err' + err;
          console.log(errorLog);
          baUtil.trackEvent('promiseFacebookLogin:$cordovaFacebook.login:fail');
          d.reject(errLog);
          return d.promise;
        }
      )
    console.log('promiseFacebookLogin:end');
    return d.promise;
  };

  // WARNING: BREAKS PLUGIN SOMETIMES
  this.promiseGetLoginStatus = function() {
    baUtil.trackEvent('promiseGetLoginStatus');
    console.log('promiseGetLoginStatus');
    var d = $q.defer();
    self = this;

    if (window.cordova && window.cordova.platformId == "browser") {
      $cordovaFacebook.browserInit(appId, version);
      // version is optional. It refers to the version of API you may want to use.
    }

    $cordovaFacebook.getLoginStatus(
      function(res) {
        baUtil.trackEvent('$cordovaFacebook.getLoginStatus:success');
        var FbToken;
        console.log('promiseGetLoginStatus:getLoginStatus:res=' + baUtil.jStr(res));
        if (res.status + "" == "connected") {
          baUtil.trackEvent('$cordovaFacebook.getLoginStatus==connected');
          FbToken = res.authResponse.accessToken;
          if (baUtil.varIsSet(FbToken)) {
            self.setFbToken(FbToken);
            console.log('promiseGetLoginStatus:getLoginStatus:resolve:FbToken=' + FbToken);
            d.resolve(res);
            return d.promise;
          } else {
            console.log('promiseGetLoginStatus:getLoginStatus:reject:FbToken==' + FbToken);
            d.reject();
            return d.promise;
          }
        } else {
          console.log('promiseGetLoginStatus:getLoginStatus:reject:connected==false');
          d.reject();
          return d.promise;
        }
      },
      function(err) {
        baUtil.trackEvent('$cordovaFacebook.getLoginStatus:err=' + err);
        console.log('$cordovaFacebook.getLoginStatus:err=' + err);
        d.reject(err);
      }
    );

    console.log('promiseGetLoginStatus:getLoginStatus:return');
    return d.promise;
  };

  /*
  this.promiseGetProfile = function() {
    console.log('promiseGetProfile');
    var d = $q.defer();
    var fbId = this.getFBUserId();
    console.log('promiseGetProfile:fbId=' + fbId);
    if (!baUtil.varIsSet(fbId)) {
      console.log('promiseGetProfile:reject:fbId=' + fbId);
      d.reject('fbId=' + fbId);
      return d.promise;
    }
    wsService.getProfileByFbId(fbId)
      .then(function(result) {
        console.log('promiseGetProfile:getProfileByFbId:result=' + this.jStr(result));
        this.getProfileRes = result;
        if (baUtil.varIsSet(result.data)) {
          console.log('promiseGetProfile:getProfileByFbId:resolve');
          this.setProfileFromWs(result);
          d.resolve(result);
          return d.promise;
        } else {
          console.log('promiseGetProfile:getProfileByFbId:reject');
          d.reject(result);
          /*
          console.log('promiseGetProfile:return:promiseRegister');
          return this.promiseRegister()
          .then(function(result)
          {
              console.log('promiseGetProfile:return:promiseGetProfile');
              return this.promiseGetProfile();
              //d.reject(result);
          });
          */
  /*
        }
      }).catch(function(err) {
        console.log('catch:promiseGetProfile:getProfileByFbId:reject');
        d.reject(err);
        return d.promise;
      });
    return d.promise;
  }
  */

  var startAppCallCount = 1;

  this.promiseLoginOrRegister = function() {
    self = this;
    var d = $q.defer();
    var startErrors = "";
    // var fbLr = this.getFbLr();
    var FbToken = this.getFbToken();
    console.log('promiseLoginOrRegister:FbToken=' + FbToken);
    if (!baUtil.varIsSet(FbToken)) {
      d.reject(FbToken);
      return d.promise;
    }
    wsService.LoginOrRegister(FbToken)
      .then(function(result) {
        console.log('promiseLoginOrRegister:LoginOrRegister:result=' + JSON.stringify(result));
        var data = result.data;
        console.log("LoginOrRegister:data.Name=" + data.Name);
        self.setProfileFromWs(result);
        d.resolve(result);
        return d.promise;
      });
    return d.promise;
  }

  // RECURSIVE WORK BACKWARDS
  this.promiseStartApp = function() {
    var self = this;
    console.log('promiseStartApp');
    var d = $q.defer();
    var doStart = true;
    console.log('promiseStartApp:call:promiseLoginOrRegister');
    return this.promiseLoginOrRegister()
      .catch(function(exc) {
        doStart = false;
        console.log('promiseStartApp:promiseLoginOrRegister:catch:exc=' + exc);
        startErrors = exc;
        if (exc === undefined) {
          startErrors = "promiseStartApp:promiseLoginOrRegister:result is empty (probably a CORS issue)";
          console.log(startErrors);
          startAppCallCount = 100; // exit, this won't be fixed          
          throw new(startErrors);
        } else {
          startErrors = JSON.stringify(exc);
        }
        return self.promiseGetLoginStatus();
      })
      .catch(function(err) {
        doStart = false;
        startErrors = JSON.stringify(err);
        console.log('promiseStartApp:promiseGetLoginStatus:catch');
        return self.promiseFacebookLogin();
      })
      .finally(function(result) {
        console.log('promiseStartApp:finally:doStart=' + doStart);
        if (doStart) {
          console.log('promiseStartApp:finally:call:startUpcoming');
          this.startUpcoming();
          if (baUtil.varIsSet(this.pushState)) {
            $state.go(this.pushState);
            this.pushState = "";
          }
          this.isAppStarted = true;
          d.resolve(result);
          return d.promise;
        } else {
          // RECURSE
          // TODO, some prob with this startAppCallCount variable
          console.log('promiseStartApp:startAppCallCount=' + startAppCallCount + ' result=' + result);
          if (startAppCallCount > 3) {
            alert('startErrors=' + $scope.startErrors);
            d.reject(result);
            return;
          }
          startAppCallCount++;
          console.log('promiseStartApp:call:promiseStartApp:RECURSE:result=' + result);
          self.promiseStartApp();
        }
      });
    return d.promise;
  }

  this.doPromiseStartAppOnce = function() {
    self = this;
    console.log('doPromiseStartAppOnce');
    // this.setDeviceInfo(ionic.Platform.device());
    var d = $q.defer();
    if (this.alreadyStarting == true) {
      window.localStorage['isFirstEntrance'] = false;
      d.reject(this.alreadyStarting);
      return false;
    }
    this.alreadyStarting = true;
    // TODO: need this but js wasn't finding it, even though I'm merely defining it here
    //console.log('doPromiseStartAppOnce:startAppCallCount='+startAppCallCount);
    window.localStorage['isFirstEntrance'] = false;

    return self.promiseStartApp();

    return d.promise;
  }

  this.startUpcoming = function() {
    console.log('startUpcoming');
    var getProfileWsr = this.getProfileFromWs();
    console.log('startUpcoming:getProfileWsr=' + getProfileWsr);
    getProfileWsr = JSON.parse(getProfileWsr);
    this.setUserId(getProfileWsr.data.UserId);
    this.updateDeviceId();
    this.setProfileFromWs(getProfileWsr);
    $state.go('app.goals-my');
    if (baUtil.varIsSet(this.pushState)) {
      $state.go(this.pushState);
      self.pushState = "";
    }
  }

  //Update device id  
  this.updateDeviceId = function() {
    var userId = this.getUserId();
    //console.log('updateDeviceId:userId='+userId);
    baUtil.trackEvent("updateDeviceId");
    var did = this.getDeviceId();
    console.log('updateDeviceId:userId' + userId + ' did=' + did);
    bwsApi.updateDeviceId(userId, did).then(function(response) {
        //Device updated successfully
        console.log('updateDeviceId:response=' + response);
        var test = response;
      },
      function(errorMessage) {
        baUtil.trackError(errorMessage, "updateDeviceId");
        console.log('updateDeviceId:errorMessage=' + this.jStr(errorMessage));
        //this.popup("Error", "Error occured in updateDeviceId");
      }

    );
  };

  this.updateNotifications = function() {
    console.log('updateNotifications');
    bwsApi.getNotification(this.getUserId()).then(function(result) {
        this.notef.count = result.data.UnReadCount;
        this.notef.list = result.data.Notifications;
      },
      function(errorMessage) {
        this.popup("Error", "Error: updateNotifications");
      });

    this.updateRequestCount();
  }

  this.updateRequestCount = function() {
    //Update request count 
    bwsApi.getRequestBoostCount(this.getUserId()).then(function(result) {
        this.requestCount = result.data;

      },
      function(errorMessage) {
        this.popup("Error", "Error: updateRequestCount");
      });

  }

  /*
   ** @result = ws result
   */
  this.setProfileFromWs = function(getProfileWsr) {
    console.log('setProfileFromWs:getProfileWsr=' + getProfileWsr);
    self.userProfile = getProfileWsr.data;
    var userId = self.userProfile.UserId;
    self.setUserId(userId);
    var profileStr;
    try {
      profileStr = JSON.stringify(getProfileWsr);
      console.log('setProfileFromWs:profileStr=' + profileStr);
      window.localStorage['profile'] = profileStr;
    } catch (err) {
      console.log('setProfileFromWs:catch:err=' + err);
    }
    //this.profile.name = this.profile.FirstName + this.profile.LastName;
    //this.profile.points = this.profile.GiveBoostPoint + this.profile.GetBoostPoint;
    console.log('setProfileFromWs:this.profileStr=' + profileStr);
    this.setMixpanelProfile(getProfileWsr.data);
    if (use_analytics) this.setGaProfile();
  };

  this.setGaProfile = function() {
    var fbid = this.getFBUserId();
    console.log('setGaProfile:fbid=' + fbid);
    analytics.setUserId(fbid);

    var UserId = this.getUserId();
    analytics.addCustomDimension(1, UserId, function(response) {
        console.log('setGaProfile:user-id:addCustomDimension:success:result=' + result);
      },
      function(error) {
        console.log('setGaProfile:user-id:addCustomDimension:fail:result=' + result);
      });

    console.log('setGaProfile:call:addCustomDimension:fbid=' + fbid);
    analytics.addCustomDimension(2, fbid, function(response) {
        console.log('setGaProfile:fbid:addCustomDimension:success:result=' + result);
      },
      function(error) {
        console.log('setGaProfile:fbid:addCustomDimension:fail:result=' + result);
      });

    ga('set', 'fbid', fbid);
    ga('set', 'User ID', UserId);
  }

  this.setMixpanelProfile = function(data) {
    var profile = this.getProfileFromWs();
    //alert('setMixpanelProfile:profile=' + JSON.stringify(profile));
    profile = JSON.parse(profile);
    var fbid = window.localStorage['fbid'];
    var userId = window.localStorage['userId'];
    this.userInfo = {
      "name": profile.Name,
      "fbid": profile.FbUserId,
      "userId": profile.UserId,
      "points": profile.LikesTotal,
      "level": profile.Level,
      "getBoostPt": profile.GetBoostPt,
      "giveBoostPt": profile.GiveBoostPt,
      "pic": profile.PicUrl,
      "email": profile.Email
    }
    this.userDeviceInfo = {
      "device_platform": ionic.Platform.device().platform,
      "dp_version": ionic.Platform.device().version,
      "cordova_version": ionic.Platform.device().cordova,
      "uuid": ionic.Platform.device().uuid,
      "device_model": ionic.Platform.device().model,
      "Platform_connection": ionic.Platform.connection,
      "Platform_platforms": JSON.stringify(ionic.Platform.platforms),
      "Platform_grade": ionic.Platform.grade,
      "Platform_ua": ionic.Platform.ua,
      "isWebView": ionic.Platform.isWebView(),
      "isIPad": ionic.Platform.isIPad()
    }
    mixpanel.people.set(this.userInfo);

    var fbid = this.getFBUserId();
    mixpanel.identify(fbid);
    mixpanel.alias(fbid);

    mixpanel.register_once({
      "userInfo": this.userInfo,
    });

    mixpanel.register({
      "userDeviceInfo": this.userDeviceInfo
    });
  }

  this.resetApp = function() {
    this.setFbToken("");
    this.setUserId("");
  }

  this.isDevMode = function() {
    return window.localStorage['isDevMode'];
  }

  this.getVersion = function() {
    return window.localStorage['boosterVersion'];
  }
  this.setVersion = function(v) {
    window.localStorage['boosterVersion'] = v;
  }

  this.setDevMode = function(b) {
    window.localStorage['isDevMode'] = b;
  }

  this.getFBUserId = function() {
    return window.localStorage['fbid'];
  };

  this.setFBUserId = function(fbid) {
    //console.log('setFBUserId:fbid='+fbid);  
    window.localStorage['fbid'] = fbid;
  };

  this.getFbFriends = function() {
    var fbFriends = window.localStorage['fbFriends'];
    //console.log('getFbFriends:fbFriends='+fbFriends);             
    return fbFriends;
  };

  this.setFbFriends = function(fbFriends) {
    //console.log('setFbFriends:fbFriends='+this.jStr(fbFriends));             
    window.localStorage['fbFriends'] = this.jStr(fbFriends);
  };

  this.getUserId = function() {
    // return 1640;
    return window.localStorage['userId'];
  };

  this.setUserId = function(userId) {
    window.localStorage['userId'] = userId;
  };

  this.getProfileFromWs = function() {
    console.log('getProfileWsr');
    var profile = window.localStorage['profile'];
    console.log('profile=' + profile);
    return profile;
  };

  this.getFbToken = function() {
    var FbToken = window.localStorage['FbToken'];
    // if (!FbToken) FbToken = "CAAI5B3QGPc0BAKAseAfO4b1zuFnN4bgFb8ZBpgz07rKGocoXIZAL0A9yhRoj7xzCbG1klHKqc9z2GQew2cdPQOpCsXjC5wzBWj56EnN0dbq8pBhwjEAlqcL4mxn0DQtmDULML9IPPUwdCcxIpTRcakB6iuHxib7yw4TEZCg2XZAr9WxEppZBn0zacP8PgSj24ASSZCmNQimAzKfEBuEuR7CwqclBHLlnDVr84ZA69x3hCBqDqO8qpevEpqGr32kwH8beeA6f7NVZBQfX8j3DCNEr";
    //console.log('getFbToken:FbToken=' + FbToken);
    return FbToken;
  }

  this.setFbToken = function(FbToken) {
    console.log('setFbToken:FbToken=' + FbToken);
    $rootScope.$emit('fbTokenSet');
    window.localStorage['FbToken'] = FbToken;
  }

  this.getFbLr = function() {
    var fbLrStr = window.localStorage['fbLr'];
    var fbLr;
    if (baUtil.varIsSet(fbLrStr)) fbLr = JSON.parse(fbLrStr);
    return fbLr;
  }

  this.setFbLr = function(res) {
    console.log('setFbLr:res=' + this.jStr(res));
    //var fbId = res.authResponse.userID;
    var fbId = res.authResponse.id;
    console.log('setFbLr:fbId=' + fbId);
    this.setFBUserId(fbId);
    window.localStorage['fbLr'] = JSON.stringify(res);
  }

  this.getDeviceId = function() {
    var deviceId = window.localStorage['did'];
    console.log('getDeviceId:deviceId=' + deviceId);
    return deviceId;
  };

  this.setDeviceId = function(deviceId) {
    this.myDeviceId = deviceId;
    var before = window.localStorage['did'];
    window.localStorage['did'] = deviceId;
    // If we just acquired a deviceId, where it was null before
    //if(!baUtil.varIsSet(before) && baUtil.varIsSet(deviceId)) 
    //{
    console.log('setDeviceId:updateDeviceId');
    this.updateDeviceId();
    //}
    console.log('setDeviceId:deviceId=' + deviceId);
  };

  var tryCordovaCount = 0;
  this.tryCordova = function() {
    self = this;
    // try right away now
    self.doPromiseStartAppOnce();
    console.log('tryCordova:tryCordovaCount=' + tryCordovaCount);
    if (tryCordovaCount > 1) {
      console.log('tryCordovaCount=' + tryCordovaCount);
      return;
      // try to move on - maybe we're on browser or..
      self.doPromiseStartAppOnce();
    }
    $timeout(function() {
      if (window.cordova !== undefined) {
        document.addEventListener('deviceready', function() {
          //if ($rs.debug > 3) console.log('run:doPromiseStartAppOnce');

          self.doPromiseStartAppOnce();
          // this.boot();
        });
      } else {
        tryCordovaCount++;
        self.tryCordova();
      }
    }, 1000);
  }

  this.PENDING = "PENDING";
  this.REQUEST = "REQUEST";
  this.COMPLETED = "COMPLETED"
});
