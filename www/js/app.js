// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
// angular.module('boosterapp', ['ionic', 'ngCordova', 'boosterapp.controllers', 'boosterapp.directives'])
angular.module('boosterapp', ['ionic', 'ngCordova', 'boosterapp.utility', 'boosterapp.services', 'boosterapp.controllers', 'boosterapp.directives'])

.run(function($ionicPlatform, loginService, baUtil, $state, stateService, $cordovaCamera, $cordovaFacebook, loginService, wsService, PushNotifications) {

  //.run(function($ionicPlatform) {
  // header('Access-Control-Allow-Origin: *');
  // header('Access-Control-Allow-Methods: GET, POST');

  // use this mostly just for dev
  loginService.resetApp();
  var codeVersion = "v2.0.0";
  var prevVersion = loginService.getVersion();
  if (prevVersion != codeVersion) {
    baUtil.trackEvent("update:" + prevVersion + ":to:" + codeVersion);
    loginService.setVersion(codeVersion);
  }

  $ionicPlatform.ready(function() {

    // if (!window.cordova) alert('not device?');
    // alert('ionicPlatform.ready');
    // var temp = Camera.DestinationType.FILE_URI;

    var dp = ionic.Platform;
    // console.log('db=' + baUtil.jStr(dp));
    baUtil.setDevicePlatform(dp);

    try {
      // alert('try login');
      $cordovaFacebook.login(["public_profile", "email", "user_friends"])
        .then(function(success) {
          // alert('login success=' + baUtil.jStr(success));
          var fbToken = success.authResponse.accessToken;
          loginService.setFbToken(fbToken);
          // loginService.promiseLoginOrRegister(fbToken);
          wsService.LoginOrRegister(fbToken).then(function(result) {
            // alert('result=' + baUtil.jStr(result));
            loginService.setProfileFromWs(result);
            $state.go('app.goals-world');
            stateService.setState('app.goals-world');
          })
        }, function(error) {
          alert('login error=' + baUtil.jStr(error));
          // error
        });
    } catch (exc) {

      //alert('catch:exc=' + baUtil.jStr(exc));
      loginService.setUserId('2637');
      // loginService.setUserId('1640');
      var fbToken = 'CAAI5B3QGPc0BADWQlY0gELWtb1iSgm6DaVQZCs8TzJIyEjQ4t9vZA3m8wAtL7YU4HbUljdKhDyZB9ZBQPEzeDqJeGhGMLFLftTa7Dikj0wJLcsLtZAEHsjKHeiwMVyLUD4JkKpO2FPS35azelRf9nYYM81EkKlCHZAZAtZAnsGuEIhmoNpwPCyZC6ZAI0dHU1q5kfKWhFcVyZAK1rFnxZAGQAkoiz75fTYv83J93yFQ3oIYHw39yRkBF7ThOm7YbIg6aLxjTE2uE81cxMVqsZBVRnqFGA';
      // var fbToken = 'CAAI5B3QGPc0BAEkCQ4ZC59LesZCzbO1Y8jL19yMLVaKRhoF4vo22WyDwVCY0kC3Bgy5ZADyysJS5uu9bcAtSeqJbPTXQSw3N5rZC072HTmtfYNm8hZAMwO7VtzatKYOmYMjYXNkPpkLh4DsSUQLxR7TfIicITHFBh2yCZBen43DWZCgZAJ2XW8U0LJfRZBVpros7hJNtcrL2zzlGMkyVZCYDe3Wt8vKrpvWCDyNgh9vuef7QukJnvnZBm4UmoCpbLokHwsZD';

      loginService.setFbToken(fbToken);

      // loginService.doPromiseStartAppOnce().then(function(result) {
      //   $state.go('app.goals-my');
      // }).catch(function(exc) {
      //   alert('could not start app:exc=' + exc);
      // });	

      //alert('catch LoginOrRegister');
      wsService.LoginOrRegister(fbToken).then(function(result) {
        //alert('catch LoginOrRegister success');
        loginService.setProfileFromWs(result);
        $state.go('app.goals-world');
      })
    }

    PushNotifications.initialize();

    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }

    //loginService.tryCordova();

  });
})

.config(function($stateProvider, $urlRouterProvider, $cordovaFacebookProvider) {

  // var appID = 625654127541709;
  // var version = "v2.2"; // or leave blank and default is v2.0
  // $cordovaFacebookProvider.browserInit(appID, version); //, version);

  var baseUrl = "http://lin-res.mapitpix.com/Booster/ionic_git/www/";

  $stateProvider
    .state('app', {
      url: "/app",
      abstract: true,
      templateUrl: baseUrl + "templates/ba-menu.html",
      controller: 'MainCtrl'
    })
    .state('app.loading', {
      url: "/loading",
      views: {
        'menuContent': {
          template: 'loading screens',
          controller: 'LoadingCtrl'
        }
      }
    })
    .state('app.loading2', {
      url: "/loading2",
      views: {
        'menuContent': {
          template: 'loading2 screens',
          controller: 'Loading2Ctrl'
        }
      }
    })
    .state('app.intro', {
      url: "/intro",
      views: {
        'menuContent': {
          template: 'intro screens',
          controller: 'IntroCtrl'
        }
      }
    })
    .state('app.goal-page', {
      url: "/goal-page",
      views: {
        'menuContent': {
          templateUrl: baseUrl + 'templates/ba-goal-page.html',
        }
      }
    })
    .state('app.photo-share-page', {
      url: "/photo-share-page",
      views: {
        'menuContent': {
          templateUrl: baseUrl + 'templates/ba-photo-share-page.html',
          controller: 'PhotoShareCtrl'
        }
      }
    })
    .state('app.choosebooster', {
      url: "/choosebooster",
      views: {
        'menuContent': {
          templateUrl: baseUrl + 'templates/ba-choose-booster.html',
          controller: 'ChooseBoosterCtrl'
        }
      }
    })
    .state('app.photo', {
      url: "/photo/:selfieId",
      views: {
        'menuContent': {
          template: "add here",
        }
      }
    })
    .state('app.photolikes', {
      url: "/photo/:selfieId/likes",
      views: {
        'menuContent': {
          templateUrl: baseUrl + "templates/ba-likes-list.html",
        }
      }
    })
    .state('app.photocomments', {
      url: "/photo/:selfieId/comments",
      views: {
        'menuContent': {
          templateUrl: baseUrl + "templates/ba-comments-list.html"
        }
      }
    })
    .state('app.goals-world', {
      url: "/goals-world",
      views: {
        'menuContent': {
          templateUrl: baseUrl + 'templates/ba-tabs-page.tpl.html',
          // templateUrl: 'templates/ba-goals.tpl.html',
          // template: "<ba-tabs-page></ba-tabs-page>",
          // controller: 'HomeCtrl',
        }
      }
    })
    .state('app.notifications', {
      url: "/notifications",
      views: {
        'menuContent': {
          templateUrl: baseUrl + 'templates/ba-notifications.html',
          controller: 'NotificationsCtrl'
        }
      }
    })
    .state('app.new-goal-summary', {
      url: "/new-goal-summary",
      views: {
        'menuContent': {
          templateUrl: baseUrl + "templates/new-goal-summary.html",
          controller: 'NewGoalSummaryCtrl'
        }
      }
    })
    .state('app.profile', {
      url: "/profile",
      views: {
        'menuContent': {
          templateUrl: baseUrl + "templates/ba-profile.html",
          controller: 'ProfileCtrl'
        }
      }
    })
    .state('app.invite', {
      url: "/invite",
      views: {
        'menuContent': {
          templateUrl: baseUrl + "templates/invite.tpl.html",
          //controller: 'InviteCtrl'
        }
      }
    })
    .state('app.about', {
      url: "/about",
      views: {
        'menuContent': {
          templateUrl: baseUrl + "templates/about.tpl.html",
          controller: 'AboutCtrl'
        }
      }
    })
    .state('app.share', {
      url: "/share",
      views: {
        'menuContent': {
          templateUrl: baseUrl + "templates/share.html"
        }
      }
    });
  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/app/goals-world');
});
