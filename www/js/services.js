'use strict';

angular.module('boosterapp.services', ['ionic'])

// ================================ STATE SERVICE ====================================
.service('stateService', function($rootScope) {
  var state;
  var mytitle;
  var who = "NOT_SET";
  var showLikesPage;
  var isShowWorldFilter;
  var statusToGet;
  var isPublic = true;
  var self = this;

  this.setState = function(theState) {
    state = theState;
    this.setStateVars(state);
    if (state === undefined) return;
    $rootScope.$emit('tabsStateChanged');
  }

  this.getState = function() {
    return state;
  }
  this.getTitle = function() {
    return mytitle;
  }
  this.getWho = function() {
    return who;
  }
  this.getShowLikesPage = function() {
    return showLikesPage;
  }
  this.getIsShowWorldFilter = function() {
    return isShowWorldFilter;
  }
  this.getStatusToGet = function() {
    return statusToGet;
  }
  this.getIsPublic = function() {
    return isPublic;
  }
  this.setPublic = function(b) {
    isPublic = b;
    if (b) {
      self.setState('app.goals-world');
    } else {
      self.setState('app.goals-friends');
    }
    $rootScope.$emit('tabsStateChanged');
  }

  this.setStateVars = function(theState) {
    var state = theState;
    if (state == 'app.likes') {
      mytitle = 'Likes';
      showLikesPage = true;
    } else {
      showLikesPage = false;
      if (state == 'app.likes') {
        mytitle = 'Likes';
        who = "My";
        statusToGet = "A"
      }
      if (state == 'app.goals-my') {
        isShowWorldFilter = false;
        mytitle = 'My Goals';
        who = "My";
        statusToGet = "A"
      }
      if (state == 'app.goals-boostees') {
        isShowWorldFilter = false;
        mytitle = "Friends's Goals";
        who = "Boostees";
        statusToGet = "A"
      }
      if (state == 'app.goals-friends') {
        mytitle = 'Explore';
        isShowWorldFilter = true;
        who = "Friends";
        statusToGet = "C"
      }
      if (state == 'app.goals-world') {
        mytitle = 'Explore';
        isShowWorldFilter = true;
        who = "World";
        statusToGet = "C"
      }
    }
    console.log('stateService:setState:state=' + state);
  }
})

// ================================ LIKE SERVICE ====================================
.service('likeService', function(wsService, baUtil, $rootScope, loginService) {
  // checking only from local cache now
  var mySelfieLikes = [];
  var selfieLikers = [];
  var selfieLikeCount = [];
  var self = this;

  this.getLikeCount = function(sid) {
    var rv = selfieLikeCount[sid];
    if (rv === undefined)
      rv = 0;
    return rv;
  }
  this.setLikeCount = function(sid, c) {
    selfieLikeCount[sid] = c;
    $rootScope.$emit('setLikeCount');
  }
  this.getLike = function(sid) {
    var found = mySelfieLikes[sid];
    return found;
  }
  this.setLike = function(sid, b, updateServer) {
    var origB = mySelfieLikes[sid];
    mySelfieLikes
      [sid] = b;
    if (origB != b) {
      $rootScope.$emit('selfieLikeCountChanged');
    }
    var fbToken = loginService.getFbToken();
    if (updateServer) {
      wsService.likeSelfie(sid, mySelfieLikes[sid], fbToken);
    }
    $rootScope.$emit('setLike');
  }
  this.flipLikeServer = function(sid) {
    var b = mySelfieLikes[sid];
    self.setLike(sid, !b, true);
    var likeCount = self.getLikeCount(sid);
    if (b)
      self.setLikeCount(sid, --likeCount);
    else
      self.setLikeCount(sid, ++likeCount);
  }
  this.flipLike = function(sid) {
      var b = mySelfieLikes[sid];
      self.setLike(sid, !b, true);
      var likeCount = self.getLikeCount(sid);
      if (b)
        self.setLikeCount(sid, --likeCount);
      else
        self.setLikeCount(sid, ++likeCount);
    }
    // checking only from local cache now
  this.getLikeClass = function(b) {
    var likeClasses = [];
    if (b) likeClasses.push('like-button-on');
    else likeClasses.push('like-button-off');
    return likeClasses;
  }
  this.updateSelfies = function(selfies) {
    var selfie = selfies[0];
    var sid = selfie.ListBoostSelfie[0].SelfieId;
    var likeCount = selfie.likecount;
    var callerLikes = selfie.CallerLikes;
    var b;
    var userId = loginService.getUserId();
    if (userId == callerLikes)
      b = true;
    else
      b = false;
    self.setLike(sid, b, false);
    self.setLikeCount(sid, likeCount);
  }

  // this.checkLike = function(selfieId, likers, userId) {
  //   if (mySelfieLikes[selfieId]) return mySelfieLikes[selfieId];
  //   if (likers.length > 0) {
  //     var isFound = false;
  //     var like = likers.filter(function(val) {
  //       // console.log('checkLike:val=' + baUtil.jStr(val));
  //       if (val._SelfieUsersLike) {
  //         var isFound = (val._SelfieUsersLike.UserId == userId);
  //         if (isFound) {
  //           console.log('likeService:checkLike:val=' + baUtil.jStr(val));
  //           mySelfieLikes[selfieId] = isFound;
  //         }
  //       }
  //     });
  //   }
  //   return isFound;
  // }

})

//================================= ITEM LOADING =====================================

.service('loadService', function($q, wsService, loginService, baUtil) {

  var items = [];
  items["My"] = [];
  items["Boostees"] = [];
  items["Friends"] = [];
  items["World"] = [];
  var who;
  var topN = 1; // not used here yet
  var skipN = 0; // not used here yet
  var moreDataCanBeLoaded = true;
  var withUpcoming = true;
  var withImage = true;
  var fbToken = loginService.getFbToken();
  var StatusToGet;

  this.getResultOData = function(r) {
    var rv = r.data.value;
    items[who].push.apply(items[who], rv);
    return rv;
  }

  this.getResultData = function(r) {
    var rv = r.data;
    items[who].push.apply(items[who], rv);
    return rv;
  }

  //fn args StatusToGet, who, withUpcoming, withImage, fbToken
  this.loadMore = function(apiPf) {
    var d = $q.defer();
    if (baUtil.d() > 5) console.log('loadService:loadMore');
    if (baUtil.d() > 50) console.log("apiPf=" + apiPf);
    apiPf().then(function(result) {
      if (baUtil.d() > 500) console.log('loadService:loadMore:success:result=' + baUtil.jStr(result));
      // skip right away for next call
      skipN += topN; // not used. using from caller. would need to inject into partial function args to use this 
      if (baUtil.d() > 5) console.log('loadService:loadMore:skipN=' + skipN + ';who=' + who);
      d.resolve(result);
    }).catch(function(exc) {
      console.log('loadMore:catch:exc=' + baUtil.jStr(exc));
    });
    return d.promise;
  }
})

// ============================ BOOST API SERVICE ===================================

.service('boostService', function($http, $filter, $cordovaDevice, $rootScope, loginService, wsService, baUtil) {

  var boost;

  this.clearBoost = function() {
    boost = {};
    boost.booster = {};
    boost.text;
    boost.datetime; //new Date(); //.format("EEE, MMM dd HH:mm a");
    boost.datetime = $filter('date')(boost.datetime, "EEE, MMM dd HH:mm a");
  }

  this.clearBoost();

  if (baUtil.devMode()) {
    boost.datetime = new Date();
    boost.datetime = $filter('date')(boost.datetime, "EEE, MMM dd HH:mm a");
    boost.isDateSet = true;
  }

  this.getFriends = function() {
    wsService.getFriends(loginService.getFbToken());
  }

  this.isSet = function() {
    if (boost.isTextSet && boost.isDateSet && boost.isBoosterSet) {
      return true;
    }
    return false;
  }
  this.setBoost = function(b) {
    boost = b;
    $rootScope.$emit('setBoost');
    // alert('setBoost:b=' + baUtil.jStr(boost));
  }
  this.getBoost = function() {
    // alert('getBoost:boost=' + baUtil.jStr(boost));
    return boost;
  }
  this.setText = function(text) {
    boost.text = text;
    boost.isTextSet = true;
  }
  this.setDateTime = function(datetime) {
    boost.datetime = datetime;
    boost.isDateSet = true;
  }
  this.setBooster = function(booster) {
    boost.booster = booster;
    boost.isBoosterSet = true;
  }
  this.getBooster = function(booster) {
    return boost.booster;
  }

})


// =============================== SELFIE SERVICE ===================================

// .service('selfieService', function() {
//   this.getSelfieIdFromGetAllBoostsItem = function(i) {
//     var item = items[i];
//     var selfieId = item.ListSelfieCommentsLikes[0].ListBoostSelfie[0].SelfieId;
//     return selfieId;
//   }
// })

// ========================= PHOTO SERVICE =====================================

.service('imageService', function(baUtil, loginService, $cordovaCamera, $ionicPlatform) {

  var selfie = {};
  selfie.shareText;
  selfie.boostId;
  selfie.comment = "";
  selfie.image = "";
  selfie.privacy = "w";
  var options;
  console.log(baUtil.jStr($ionicPlatform));
  this.dp = baUtil.getDevicePlatform();
  var self = this;

  this.getSelfie = function() {
    return selfie;
  }
  this.setSelfie = function(photo) {
    selfie.image = photo;
  }
  this.setPrivacy = function(p) {
    selfie.privacy = p;
  }

  this.chooseFile = function() {
    alert('chooseFile');
    fileChooser.open(function(uri) {
      alert(uri);
    });
  }

  this.initOptions = function() {
    options = {
      quality: 50,
      destinationType: Camera.DestinationType.FILE_URI,
      sourceType: Camera.PictureSourceType.CAMERA,
      allowEdit: true,
      encodingType: Camera.EncodingType.JPEG,
      targetWidth: 640,
      targetHeight: 640,
      popoverOptions: CameraPopoverOptions,
      saveToPhotoAlbum: false,
      cameraDirection: 1
    };

    self.dp = baUtil.getDevicePlatform();

    //alert('dp='+dp);
    if (self.dp == "iOS") {
      options = {
        quality: 50,
        destinationType: Camera.DestinationType.DATA_URL,
        sourceType: Camera.PictureSourceType.CAMERA,
        allowEdit: true,
        targetWidth: 640,
        targetHeight: 640,
        encodingType: Camera.EncodingType.JPEG,
        mediaType: 0,
        correctOrientation: false,
        saveToPhotoAlbum: false,
        popoverOptions: CameraPopoverOptions,
        cameraDirection: 1
      };
      //alert('options='+JSON.stringify(options));
    }
  }

  this.takePhoto = function() {
    if (!options) initOptions();
    $cordovaCamera.getPicture(options).then(function(imageUri) {
      // Success! Image data is here
      if (self.dp == "iOS") {
        selfie.image = imageUri;
      } else {
        //$scope.boosterAddImageToCanvas(imageUri);
        self.uriToBase64(imageUri).then(function(result) {
          console.log('getPicture:uriToBase64:result=' + result);
          selfie.image = result;
          //$scope.$apply();
        });
      }
    }, function(err) {
      // An error occured. Show a message to the user
      //alert('getPicture:err='+err);
      baUtil.trackError(err, "getPicture");
    });
  }

  this.addImageToCanvas = function(croppedImageId) {
    var canvas, context;
    $scope.boosterAddImageToCanvas = function(imageUri) {
      canvas = document.getElementById(croppedImageId);
      context = canvas.getContext('2d');
      make_base();

      function make_base() {
        base_image = new Image();
        base_image.src = imageUri;
        context.drawImage(base_image, 100, 100);
        //$scope.$apply();
      }
    }
  }

  this.uriToBase64 = function(fileUri) {
    var d = $q.defer();
    window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, gotFS, fail);

    function gotFS(fileSystem) {
      var fullPath = fileUri;
      var relPath = fullPath.replace('file:\/\/\/storage\/emulated\/0\/', '');
      // REMOVE UP TO Android
      window.resolveLocalFileSystemURI(relPath, winConvert, failConvert);

      function winConvert(uri) {
        console.log('uriToBase64:winConvert2:uri=' + uri);
      }

      function failConvert(error) {
        console.log('uriToBase64:failConvert:error.code=' + error.code);
        d.reject(error);
      }
      fileSystem.root.getFile(relPath, null, gotFileEntry, fail);
    }

    function gotFileEntry(fileEntry) {
      console.log('uriToBase64:gotFileEntry:fileEntry=' + fileEntry);
      fileEntry.file(gotFile, fail);
    }

    function gotFile(file) {
      console.log('gotFile');
      readDataUrl(file);
    }

    function readDataUrl(file) {
      var reader = new FileReader();
      reader.onloadend = function(evt) {
        //console.log('uriToBase64:readDataUrl:evt.target.result='+evt.target.result);
        var base64Image = evt.target.result.replace('data:image\/jpeg;base64,', '');
        selfie.image = base64Image;
        //console.log('uriToBase64:readDataUrl:$scope.selfie.image='+$scope.selfie.image);
        //$scope.$apply();
        d.resolve(base64Image);
        return d.promise;
      };
      reader.readAsDataURL(file);
    }

    function fail(error) {
      console.log('uriToBase64:fail:error.code=' + error.code);
    }
    return d.promise;
  }

})

// ============================  WS API SERVICE ===============================

.service('shareImageService', function(baUtil, wsService, boostService, imageService) {
  this.sendPhoto = function(doShare) {
    baUtil.trackEvent("sendPhoto");
    baUtil.showLoading();
    wsService.sendPhoto(selfie.boostId, selfie.comment, selfie.image, selfie.isPublic).then(function(result) {
        baUtil.hideLoading();
        baUtil.trackSendPhoto(selfie.comment, selfie.boostId);
        $cordovaToast.showShortCenter('Photo sent successfully!').then(function(success) {
          if (doShare == true) {
            if ($rs.debug > 0)
              console.log('TakePhotoBoostCtrl:sendPhoto:fbShare:$scope.selfie.UserId' + $scope.selfie.UserId);
            selfie.message = "@" + boostService.boost.booster.RequestUserName + " boosted me to " + boostService.boost.text;
            baUtil.fbShare(selfie).then(function(result) {
              baUtil.trackEvent('sendPhoto:fbShare:result');
              console.log("$state.go('app.completed')");
              $state.go('app.completed');
            });
          }
          console.log("$state.go('app.completed')");
          $state.go('app.completed');
        }, function(error) {
          // error
          baUtil.trackError(error, "showShortCenter:send_selfie");
        });
      },
      function(errorMessage) {
        baUtil.hideLoading()
        baUtil.trackError(errorMessage, "sendPhoto");
        if ($rs.debug > 0)
          console.log("Error", "Error occured when sending selfie");
      });
  }
})

// ============================ BOOSTER WS API SERVICE ===============================

.service('wsService', function($http, $cordovaDevice, $rootScope, baUtil) {
  var baseUrl = "http://booster.mapapin.com/api/";
  //var baseUrl = "localhost:2456/api/";
  // var baseUrl = "http://api.theboosterapp.com/api/";
  // var baseUrl = "http://10.211.55.8/help";
  // var baseUrl = "http://10.0.0.8/help";

  this.getUrl = function(theUrl) {
    return $http({
      method: 'GET',
      url: theUrl
    });
  }

  this.LoginOrRegister = function(FbToken) {
    return $http({
      method: 'GET',
      url: baseUrl + 'User/LoginOrRegister?FbToken=' + FbToken
        // data: {
        //   FbToken: FbToken
        // }
    });
  }

  //use for e.g. FB Graph to get friends
  this.getUrl = function(theUrl) {
    return $http({
      method: 'GET',
      url: theUrl
    });
  }

  this.getFriends = function(fbToken) {
    return $http({
      method: 'GET',
      url: baseUrl + "User/GetFriends?FbToken=" + fbToken
    });
  }

  this.updateDeviceId = function(userId, deviceId) {

    var device = ionic.Platform.device();
    var uuid = device.uuid;
    window.localStorage.setItem("uuid", uuid);
    var platform = ionic.Platform.device().platform;
    var qString = "userId=" + userId + "&deviceId=" + deviceId + "&platform=" + platform + '&uuid=' + uuid; //Note: for android "A", Ios "I";
    var theUrl = baseUrl + "User/UpdateDeviceId?" + qString;
    console.log('updateDeviceId:theUrl=' + theUrl);
    return $http({
      method: 'GET',
      url: theUrl
    });

  }

  this.sendComment = function(selfieId, fbToken, comment) {
    return $http({
      method: 'POST',
      url: baseUrl + "Boost/CommentSelfie",
      data: {
        selfieId: selfieId,
        FbToken: fbToken,
        Comment: comment
      }
    });
  };

  this.likeSelfie = function(selfieId, like, fbToken) {
    return $http({
      method: 'POST',
      url: baseUrl + "Boost/LikeSelfie",
      data: {
        selfieId: selfieId,
        LikeStatus: like,
        FbToken: fbToken
      }
    });
  };

  this.getSelfieComments = function(SelfieId, fbToken, topN, skipN) {
    return $http({
      method: 'GET',
      url: baseUrl + "Boost/GetSelfieComments?SelfieId=" + SelfieId + "&FbToken=" + fbToken + "&topN=" + topN + "&skipN=" + skipN
    });
  }

  this.getLikesCount = function(SelfieId, fbToken) {
    return $http({
      method: 'GET',
      url: baseUrl + "Boost/GetLikesCount?SelfieId=" + SelfieId + "&FbToken=" + fbToken
    });
  }

  this.getSelfieLikes = function(SelfieId, fbToken, topN, skipN) {
    return $http({
      method: 'GET',
      url: baseUrl + "Boost/GetSelfieLikes?SelfieId=" + SelfieId + "&FbToken=" + fbToken + "&topN=" + topN + "&skipN=" + skipN
    });
  }

  // make the right odata query for 'who'
  this.getGoals = function(who, status, top, skip, fbToken) {
    var uid = loginService.getUserId();
    var role;
    var path;
    var args1 = "odata/BoostRequests?$filter=Status eq '" + status + "'";
    var args2 = "&$orderby=RequestDate%20desc&$top=" + top + "&$skip=" + skip;
    path = args1;

    if (who == "my")
      role = "and UserId";
    if (who == "boostees")
      role = "and BoostId";

    if (role) path += role + "%20eq%20" + uid

    if (who == "friends") {
      // need to join with FbFriends table here
      // path = args1 + '$filter=' + uid + args2
    }

    path += args2

    var url = baseUrl + path;
    console.log('url=' + url);

    return $http({
      method: 'GET',
      url: baseUrl + path
    });
  }

  this.getAllBoosts = function(who, getUpcoming, withImage, top, skip, fbToken) {
    if (fbToken === undefined) {
      console.log('getAllBoosts:fbToken=' + fbToken);
      return;
    }
    var theUrl = baseUrl + "Boost/GetAllBoosts?who=" + who + "&getUpcoming=" + getUpcoming + "&withImage=" + withImage + "&top=" + top + "&skip=" + skip + "&FbToken=" + fbToken;
    console.log('theUrl=' + theUrl);
    //alert('theUrl=' + theUrl);
    return $http({
      method: 'GET',
      url: theUrl
    });
  }

  this.getBoostSelfies = function(userId, withImage, top, skip, fbToken) {
    return $http({
      method: 'GET',
      url: baseUrl + "Boost/GetBoostSelfies?userId=" + userId + "&withImage=" + withImage + "&top=" + top + "&skip=" + skip
    });
  }

  this.getCompletedBoost = function(userId, top, skip) {
    return $http({
      method: 'GET',
      url: baseUrl + "Boost/GetCompletedBoost?userId=" + userId + "&top=" + top + "&skip=" + skip
    });
  }

  this.addBoost = function(fromUserId, toUserId, date, time, title, alert) {
    return $http({
      method: 'POST',
      url: baseUrl + "Boost/CreateBoostRequest",
      data: {
        UserId: fromUserId,
        ToFbUserId: toUserId,
        Title: title,
        EventDate: date,
        TzOffset: time,
        Alert: alert
      }
    });
  }

  this.getBoostDetails = function(boostId) {
    return $http({
      method: 'GET',
      url: baseUrl + "Boost/GetBoostDetail?boostId=" + boostId
    });
  }

  this.markNotfRead = function(userId) {
    return $http({
      method: 'GET',
      url: baseUrl + "Boost/MarkNotfRead?userId=" + userId
    });
  }

  this.updateBoost = function(boostId, fromUserId, toUserId, dateTime, alert) {
    return $http({
      method: 'POST',
      url: baseUrl + "AddBoost",
      data: {
        boostId: boostId,
        fromUserId: fromUserId,
        toUserId: toUserId,
        dateTime: dateTime,
        alert: alert
      }
    });
  }

  this.sendSelfie = function(boostId, comment, privacy, imageData) {
    // var who = "f"; // friends only
    // if (isPublic) who = "w";
    var isDev = false;
    if (baUtil.devMode()) {
      isDev = true;
    }
    return $http({
      method: 'POST',
      url: baseUrl + "Boost/SendSelfie",
      data: {
        BoostId: boostId,
        Comment: comment,
        Image: imageData,
        Privacy: privacy,
        isDev: isDev
      }
    });
  }

  this.getSelfieById = function(selfieId, who, withImage, fbToken) {
    return $http({
      method: 'GET',
      url: baseUrl + "Boost/GetSelfieById?selfieId=" + selfieId + "&who=" + who + "&withImage=" + withImage + "&fbToken=" + fbToken
    });
  }

  this.getSelfie = function(boostId) {
    return $http({
      method: 'GET',
      url: baseUrl + "Boost/GetSelfieByBoostId?boostId=" + boostId
    });
  }

  this.getNotification = function(userId) {
    return $http({
      method: 'GET',
      url: baseUrl + "Boost/GetNotficatios?userId=" + userId
    });
  }

  this.sendPoke = function(boostId) {
    var theUrl = baseUrl + 'Boost/SendPoke?boostId=' + boostId;
    return $http({
      method: 'GET',
      url: theUrl
    });
  }

  this.getDefaultBoosters = function() {
    return $http({
      method: 'GET',
      url: baseUrl + "User/GetDefaultBooster"
    });
  }


})


// ========================= PUSH NOTIFICATION SERVICE =====================================

.service('PushNotifications', function(baUtil, $cordovaPush, loginService, $location, $ionicPlatform, $cordovaToast, $rootScope, $state) {

  // alert('PushNotifications');

  var self = this;
  var pushPlugin = null;

  var itemPushed = {};

  this.getItem = function() {
    return itemPushed;
  }
  this.setItem = function(item) {
    itemPushed = item;
    $rootScope.$emit('pushSetItem');
  }

  this.initialize = function() {

    // alert('PushNotifications:initialize');
    pushPlugin = window.plugins.pushNotification;
    // alert('PushNotifications:pushPlugin');

    var dp = baUtil.getDevicePlatform();
    if (dp == 'android' || dp == 'Android' || dp == "amazon-fireos") {
      pushPlugin.register(
        function(result) {
          console.log('PushNotifications:initialize:1:result=' + result);
        },
        function(result) {
          alert('PushNotifications:initialize:2:result=' + result);
          console.log('PushNotifications:initialize:2:result=' + JSON.stringify(result));
        }, {
          "senderID": "220739435041",
          "ecb": "onNotificationGCM"
        });

    } else if (dp == 'iOS') {
      pushPlugin.register(
        function(result) {
          console.log('pushPlugin.register:1:result=' + JSON.stringify(result));
          loginService.setDeviceId(result)
        },
        function(result) {
          alert('pushPlugin.register:2:result=' + JSON.stringify(result));
        }, {
          "badge": "true",
          "sound": "true",
          "alert": "true",
          "ecb": "onNotificationAPN"
        });
    }

    // notifications for Android
    window.onNotificationGCM = function(e) {
      window.boosterNotification = e;
      switch (e.event) {
        case 'registered':
          if (e.regid.length > 0) {
            loginService.setDeviceId(e.regid);
          }
          break;

        case 'message':
          // if this flag is set, this notification happened while we were in the foreground.
          // you might want to play a sound to get the user's attention, throw up a dialog, etc.
          if (e.foreground) {
            // on Android soundname is outside the payload. 
            // On Amazon FireOS all custom attributes are contained within payload
            var soundfile = e.soundname || e.payload.sound;
            // if the notification contains a soundname, play it.
            //var my_media = new Media("/android_asset/www/"+ soundfile);
            //my_media.play();
          } else { // otherwise we were launched because the user touched a notification in the notification tray.
            if (e.coldstart) {
              //
            } else {
              //
            }
          }

          self.sendToScreen(e.payload);

          /*
        var msg = e.payload.message.replace(/<b>/g, "")
        msg = msg.replace(/<\/b>/g, "");
        $cordovaToast.showShortCenter(msg).then(function(success) {
                        //$state.go('app.upcoming');
                        loginService.updateNotifications();
                      }, function (error) {
                        // error
                  }
                );
        */

          //if(loginService.debug>0) console.log(e.payload.message); 
          //Only works for GCM
          // e.payload.msgcnt + '</li>');
          //Only works on Amazon Fire OS
          // e.payload.datetimeStamp
          break;

        case 'error':
          //e.msg 
          break;

        default:
          // Unknown
          break;
      }

    };

    // notifications for iOS
    window.onNotificationAPN = function(result) {
      alert('onNotificationAPN:result=' + baUtil.jStr(result));
      if (baUtil.d() > 3) console.log('onNotificationAPN:result=' + baUtil.jStr(result));
      //if(loginService.debug>0) console.log('onNotificationAPN:result='+JSON.stringify(result));

      /*
      if ( event.alert )
        {
            //navigator.notification.alert(event.alert);
        }

        if ( event.sound )
        {
            //var snd = new Media(event.sound);
            //snd.play();
        }

        if ( event.badge )
        {
            //.setApplicationIconBadgeNumber(successHandler, errorHandler, event.badge);
        }
        */

      loginService.sendToScreen(result);

    };

  };

  this.handleNotification = function(buttonIndex) {
    console.log('handleNotification:buttonIndex=' + buttonIndex);

    if (buttonIndex == 2) // if CANCEL, return
    {
      return;
    }

    // else, goto screen
    if (baUtil.varIsSet(loginService.isAppStarted)) {
      if (baUtil.varIsSet(pushState)) {
        if (baUtil.varIsSet(pushTarget)) {
          $state.go(pushState, {
            boostId: pushTarget
          });
        } else {
          $state.go(pushState);
        }
      }
    }
  }

  this.sendToScreen = function(result) {
    console.log('new sendToScreen:result=' + JSON.stringify(result));
    var nType = result.nType;
    var targetId = result.targetId;
    var firstName = result.firstName;
    var boostName = result.boostName;
    console.log('sendToScreen:nType=' + nType);
    console.log('sendToScreen:targetId=' + targetId);
    console.log('sendToScreen:firstName=' + firstName);
    console.log('sendToScreen:boostName=' + boostName);

    var msg;
    if (nType == "AcceptBoost") {
      //r = window.confirm(''+firstName + ' accepted your request for '+boostName+'! Check it now?');
      msg = firstName + ' accepted your request for ' + boostName + '! Check it now?';
      //pushState = 'app.upcoming';
      pushState = 'app.selfie';
      pushTarget = result.targetId;
      //$location.path("/app/selfie/" + item.TargetId + "/true");
    } else if (nType == "Boost") {
      msg = firstName + ' Boosted you to ' + boostName + '! Check it now?';
      //pushState = 'app.upcoming';
      pushState = 'app.selfie';
      pushTarget = result.targetId;
      //$location.path("/app/selfie/" + item.TargetId + "/true");
    } else if (nType == "RejectBoost") {
      msg = firstName + ' rejected your request for ' + boostName + '! Check it now?';
      pushState = 'app.upcoming';
    } else if (nType == "BoostRequest") {
      msg = firstName + ' requested a boost to ' + boostName + '! Check it now?';
      pushState = 'app.upcoming';
    } else if (nType == "Selfie") {
      msg = firstName + ' sent you a selfi for ' + boostName + '! Check it now?';
      // pushState = 'app.completed';
      pushState = 'app.review';
      pushTarget = result.targetId;
      //$location.path("/app/review/" + item.TargetId + "/true");
    } else if (nType == "Review") {
      msg = firstName + ' sent you a review for ' + boostName + '! Check it now?';
      pushState = 'app.completedreview';
      pushTarget = result.targetId;
      //$location.path("/app/creview/" + item.TargetId + "/true");
    } else if (nType == "UpdateBoost") {
      msg = firstName + ' send you an update for ' + boostName + '! Check it now?';
      pushTarget = result.targetId;
    } else if (nType == "Time") {
      msg = 'It is time for ' + firstName + ' to ' + boostName + '! Check it now?';
    }

    console.log('PushNotifications:sendToScreen:result=' + baUtil.jStr(result));
    alert('PushNotifications:sendToScreen:result=' + baUtil.jStr(result));
    self.setItem(result);

    console.log("sendToScreen:pushState=" + pushState);
    console.log("sendToScreen:pushTarget=" + pushTarget);

    window.boosterNotification = result;

    navigator.notification.confirm(msg, handleNotification, nType, ["Ok", "Cancel"])

  }


});
