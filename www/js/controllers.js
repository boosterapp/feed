angular.module('boosterapp.controllers', [])

.controller('MainCtrl', function($scope, $ionicModal, $timeout) {
  // Form data for the login modal
  $scope.loginData = {};

  // Create the login modal that we will use later
  $ionicModal.fromTemplateUrl('templates/login.html', {
    scope: $scope
  }).then(function(modal) {
    $scope.modal = modal;
  });

  // Triggered in the login modal to close it
  $scope.closeLogin = function() {
    $scope.modal.hide();
  };

  // Open the login modal
  $scope.login = function() {
    $scope.modal.show();
  };

  // Perform the login action when the user submits the login form
  $scope.doLogin = function() {
    if (baUtil.d() > 5) console.log('Doing login', $scope.loginData);

    // Simulate a login delay. Remove this and replace with your login
    // code if using a login system
    $timeout(function() {
      $scope.closeLogin();
    }, 1000);
  };
})

.controller('LoadingCtrl', function($scope, $state, $rootScope, $ionicPlatform, loginService, baUtil, $ionicSlideBoxDelegate, PushNotifications) {
  baUtil.trackEvent('LoadingCtrl');
  //loginService.doPromiseStartAppOnce();
})

// AFTER FB LOGIN
.controller('Loading2Ctrl', function($scope, $state, $rootScope, $ionicPlatform, loginService, baUtil, $ionicSlideBoxDelegate, PushNotifications) {
  baUtil.trackEvent('Loading2Ctrl');
  loginService.doPromiseStartAppOnce();
})

.controller('IntroCtrl', function($scope, $state, $rootScope, $ionicPlatform, $cordovaFacebook, loginService, baUtil, $ionicSlideBoxDelegate, PushNotifications) {

  $cordovaFacebook.login(["public_profile", "email", "user_friends"])
    .then(function(success) {
      alert('fb login success!');
      // { id: "634565435",
      //   lastName: "bob"
      //   ...
      // }
    }, function(error) {
      // error
    });

  baUtil.trackEvent('IntroCtrl');
  $scope.facebookLogin = function() {
      $state.go('app.loading2');
    }
    //Handle the slide
  $scope.nextSlide = function() {
    $rootScope.trackEvent("intro_screen_num=" + $ionicSlideBoxDelegate.currentIndex());
    if ($ionicSlideBoxDelegate.currentIndex() == $ionicSlideBoxDelegate.slidesCount() - 1) {
      //$ionicSlideBoxDelegate.slide(0);  
      //$rs.alreadyStarting = false;
      //$state.go('app.loading2');
      //$rootScope.doPromiseStartAppOnce();
      $rootScope.impl_facebookLogin();
    } else {
      $ionicSlideBoxDelegate.next();
    }
  };
  $scope.$on('$viewContentLoaded', function() {
    $rootScope.doPromiseStartAppOnce();
  });
})

.controller('HomeCtrl', function($scope, $stateParams, $state, $rootScope, baUtil) {
  $rootScope.$on('$stateChangeStart',
    function(event, toState, toParams, fromState, fromParams) {
      console.log('HomeCtrl:stateChangeStart:fromState=' + baUtil.jStr(fromState) + ";toState=" + baUtil.jStr(toState));
      // $scope.offset = 0;
      // $scope.loadMore();
      // event.preventDefault();
      // transitionTo() promise will be rejected with 
      // a 'transition prevented' error
    })
})

.controller('NotificationsCtrl', function($scope, $stateParams) {})

.controller('ChooseBoosterCtrl', function($scope, $stateParams) {
  // alert('ChooseBoosterCtrl');
})

.controller('NewGoalSummaryCtrl', function($scope, $rootScope, $state, $stateParams, $filter, $cordovaDatePicker, $ionicPopup, loginService, wsService, boostService, baUtil) {
  $scope.boost = {};

  var datePicker = $cordovaDatePicker;

  $scope.sendBoost = function() {
    var isSet = boostService.isSet();
    if (!isSet) {
      alert("please fill in the 3 forms");
    } else {
      wsService.addBoost(loginService.getProfile().UserId, $scope.boost.booster.FbId, $scope.boost.datetime, $scope.getTimeZone(), $scope.boost.text, -15).then(function(result) {
        boostService.clearBoost();
        $state.go('app.goals-world');
      });
    }
  };

  $scope.getTimeZone = function() {
    return -120;
  }

  $scope.chooseBooster = function() {
    // alert('chooseBooster');
    $state.go('app.choosebooster');
  }

  $scope.getBoost = function() {
    $scope.boost = boostService.getBoost();
    if (baUtil.d() > 5) console.log('NewGoalSummaryCtrl:$scope.boost=' + baUtil.jStr($scope.boost));
  }

  $scope.getBoost();
  $scope.$on('$stateChangeSuccess', function() {
    $scope.boost = boostService.getBoost();
  });

  $scope.openGoalPopup = function() {
    $scope.myPopup = $ionicPopup.show({
      templateUrl: 'templates/popup-goal.tpl.html',
      scope: $scope,
    });
    cordova.plugins.Keyboard.show();
  };

  $scope.closeBtn = function() {
    $scope.myPopup.close();
    cordova.plugins.Keyboard.close();
    $scope.checkValidation();
    var isSet = boostService.isSet();
    if (!isSet) {
      $scope.openDatePopup();
    }
  };

  $scope.openDatePopup = function() {
    $scope.myDatePopup = $ionicPopup.show({
      templateUrl: 'templates/popup-date.tpl.html',
      scope: $scope,
      // buttons: [
      //   { text: 'Cancel' },
      //   {
      //     text: '<b>Done</b>',
      //     type: 'button-positive',
      //     onTap: function() {
      //       // alert('date send');
      //       $state.go('app.choosebooster');
      //     }
      //   },
      // ]
    });
    // $scope.myDatePopup.then(function(res) {
    //   ionicPopup.close();
    //   $scope.hideKeyboard();

    // });
    // $scope.hideKeyboard();
    cordova.plugins.Keyboard.close();
  }

  $scope.goBtn = function() {
    var text = document.getElementById("textarea").value;
    boostService.setText(text);
    $scope.boost = boostService.getBoost();
    $scope.myPopup.close();
    cordova.plugins.Keyboard.close();
    $scope.showDatetime();
    $scope.checkValidation();
  };

  $rootScope.$on('$stateChangeStart',
    function(event, toState, toParams, fromState, fromParams) {
      console.log('NewGoalSummaryCtrl:stateChangeStart:fromState.url=' + fromState.url + ";toState.url=" + toState.url);
      if (toState.url == "/new-goal-summary") {
        $scope.openGoalPopup();
      }
    });

  $scope.showDatetime = function() {
    var cordovaDatePicker = $cordovaDatePicker;
    var dp = baUtil.getDevicePlatform();
    if (dp == "iOS") {
      var options = {
        date: new Date(),
        mode: 'datetime',
        allowOldDates: false,
        backgroundAlpha1: 0.4,
        backgroundAlpha2: 0.4
      };

      cordovaDatePicker.show(options).then(function(dto) {
        var newDate = new Date(dto);
        var sdatetime = $filter('date')(newDate, "EEE, MMM dd HH:mm a");
        boostService.setDateTime(sdatetime);
        $scope.boost = boostService.getBoost();
        // alert('$scope.boost=' + $scope.boost);
        // $scope.boost.datetime = $filter('date')(boost.datetime, "EEE, MMM dd HH:mm a");
        // alert('2$scope.boost=' + $scope.boost);
        $scope.myDatePopup.close();
        cordova.plugins.Keyboard.close();

        var isSet = boostService.isSet();
        if (!isSet) {
          $scope.chooseBooster
        }

      });

      datePicker._dateSelectionCanceled = function() {
        $scope.myDatePopup.close();
        cordova.plugins.Keyboard.close();
      }

      // datePicker._dateSelected = function(timeIntervalSince1970) {
      //   var boostDate = new Date(timeIntervalSince1970 * 1000);
      //   var sdatetime = boostDate.format('yyyy-mm-dd HH:MM');
      //   boostService.setDateTime(sdatetime);
      //   $scope.boost = boostService.getBoost();
      //   $scope.myDatePopup.close();
      //   $scope.checkValidation();
      //   // $state.go('app.choosebooster');
      // }

      datePicker._dateChanged = function(timeIntervalSince1970) {
        var boostDate = new Date(timeIntervalSince1970 * 1000);
        var sdatetime = boostDate.format('yyyy-mm-dd HH:MM');
        boostService.setDateTime(sdatetime);
        $scope.boost = boostService.getBoost();
      }

      cordova.plugins.Keyboard.close();

    } else {
      var options = {
        date: new Date(),
        mode: 'datetime',
        allowOldDates: false
      };
      cordovaDatePicker.show(options).then(function(dto) {
        var sdate = dto.format('yyyy-mm-dd ');
        if (baUtil.d() > 5) console.log('showDatetime:show:sdate=' + sdate);
        $scope.boost.evtDateTime = sdate;
        boostService.setDateTime(sdate);
        $scope.boost = boostService.getBoost();
        //$scope.$apply();

        var optionsTime = {
          date: new Date(),
          mode: 'time',
          allowOldDates: true
        };
        cordovaDatePicker.show(options).then(function(dto2) {
          var stime = dto2.format('HH:MM');
          if (baUtil.d() > 5) console.log('showDatetime:show:stim=' + stime);
          var sdate = $scope.boost.datetime;
          boostService.setDateTime(sdatetime + stime);
          $scope.boost = boostService.getBoost();
        });
      });
    }
  }
})

.controller('PhotoShareCtrl', function($scope, $stateParams, wsService, loginService) {
  // alert("PhotoShareCtrl");
})

.controller('ProfileCtrl', function($scope, $stateParams, wsService, loginService) {
  $scope.FbToken = loginService.getFbToken();

  if ($scope.FbToken == 'undefined') {
    alert('ProfileCtrl:$scope.FbToken=' + $scope.FbToken);
    loginService.setFbToken($scope.FbToken);
  }

  // alert('ProfileCtrl:call:getProfile');
  // wsService.getProfile(loginServe.getUserId()).then(function(result) {
  //   alert('getProfile:result=' + result);
  //   $scope.profile = result;
  // }, function(exc) {
  //   alert('getProfile:exc=' + baUtil.jStr(exc));
  // });

  // loginService.promiseFacebookLogin().then(function(result) {
  //   $scope.FbToken = loginService.getFbToken();
  // });
})

.controller('InviteCtrl', function($scope, $stateParams) {})

.controller('AboutCtrl', function($scope, $stateParams) {})

// .controller('BaScrollCtrl', function($scope, $state, $stateParams, wsService, $timeout) {
//   $scope.items = [];
//   var counter = 0;
//   $scope.loadMore = function() {
//     // alert('BaScrollCtrl:loadMore:counter=' + counter);
//     for (var i = 0; i < 50; i++) {
//       $scope.items.push({
//         id: counter
//       });
//       counter += 1;
//     }
//     $scope.$broadcast('scroll.infiniteScrollComplete');
//     $scope.$apply();
//     // alert('BaScrollCtrl:loadMore:2:counter=' + counter);
//   };
//   $scope.loadMore();
// })

// .controller('InfiniteCtrl', function($scope, $state, $stateParams, baUtil, loginService, wsService) {
//   $scope.items = [];
//   $scope.top = 1;
//   $scope.offset = 0;
//   $scope.loadMore = function() {
//     // $http.get('/more-items').success(function(items) {
//     $scope.offset += $scope.top;
//     if (baUtil.d() > 5) console.log('$scope.offset=' + $scope.offset);
//     wsService.getCompletedBoost(loginService.getUserId(), $scope.top, $scope.offset).then(function(result) {
//       $scope.items.push(result.data);
//       $scope.$broadcast('scroll.infiniteScrollComplete');
//       $scope.$broadcast('scroll.resize');
//     });
//   };
//   $scope.$on('$stateChangeSuccess', function() {
//     $scope.loadMore();
//   });
// })
